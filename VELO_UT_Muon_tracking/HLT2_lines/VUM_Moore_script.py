from Moore import options, run_moore
from Hlt2Conf.lines.charmonium_to_dimuon import JpsiToMuMu_line, Psi2SToMuMu_line
from Hlt2Conf.probe_muons import make_velomuon_muons, make_veloutmuon_muons #make_veloutmuon_muons_v2 #make_veloutmuonsoa_muons
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.reconstruction_objects import reconstruction as reco
from PyConf import configurable
from Moore.lines import Hlt2Line
from Hlt2Conf.lines.config_pid import nopid_muons
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon
from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.hlt2_tracking import make_TrackBestTrackCreator_tracks
import Functors as F
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
from Functors.math import in_range
from PyConf.packing import persistreco_writing_version
from PyConf.Algorithms import PrHybridSeeding, MuonProbeToLongMatcher
from RecoConf.legacy_rec_hlt1_tracking import (make_reco_pvs, make_PatPV3DFuture_pvs)
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks)
from RecoConf.hlt2_global_reco import (reconstruction, make_fastest_reconstruction)


# MC files and properties
conddb_tags = {"pp":   "sim-20231017-vc-mu100",
               "PbPb": "sim-20231017-vc-md100"}

dddb_tags = {"pp":   "dddb-20231017",
             "PbPb": "dddb-20231017"}

input_files = {"JpsiToMuMu": {"pp_nu1.6": ['root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000003_1.digi',
                                           'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000040_1.digi',
                                           'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000042_1.digi',
                                           "root://x509up_u147315@lhcbdcache-kit.gridka.de:1094//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000059_1.digi",
                                           "root://x509up_u147315@lhcbdcache-kit.gridka.de:1094//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000128_1.digi",
                                           "root://x509up_u147315@lhcbdcache-kit.gridka.de:1094//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000131_1.digi",
                                          ],
                              
                              "pp_nu7.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000015_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000017_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000024_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000046_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000056_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000080_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000171_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000204_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000239_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000257_1.digi",
                                           "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00212730/0000/00212730_00000361_1.digi"
                                          ],
                              
                             "PbPb_peripheral": ['root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000056_1.xdigi',
                                                 'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000162_1.xdigi',
                                                 'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000176_1.xdigi',
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000070_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000278_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000419_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000467_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000656_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000839_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000840_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000859_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000975_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00001100_1.xdigi",
                                                 "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00001250_1.xdigi",
                                                 "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000013_1.xdigi",
                                                 "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000168_1.xdigi",
                                                 "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000194_1.xdigi",
                                                 "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000195_1.xdigi",
                                                 ]
                                                 ,
                             
                             "PbPb_central": []
                            },
               
                "Psi2SToMuMu": {"pp_nu1.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217989/0000/00217989_00000008_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217989/0000/00217989_00000013_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217989/0000/00217989_00000021_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217989/0000/00217989_00000023_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217989/0000/00217989_00000028_1.digi",
                                            ],
                                
                                "pp_nu7.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000036_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000052_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000070_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000106_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000113_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000166_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000171_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000174_1.digi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000414_1.digi",
                                             "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000028_1.digi",
                                             "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000040_1.digi",
                                             "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000207_1.digi",
                                             
                                            ]
                               },
                
                "Upsi1SToMuMu": {"pp_nu7.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000017_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000018_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000021_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000030_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000041_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000045_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000046_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000047_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000098_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000102_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000110_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000120_1.digi",
                                             ]
                                },
                
                "Upsi2SToMuMu": {"pp_nu7.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000036_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000039_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000046_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000049_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000062_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000089_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000187_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000190_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000191_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000196_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000201_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218175/0000/00218175_00000231_1.digi",
                                             ]
                                },
                
                "Upsi3SToMuMu": {"pp_nu7.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000025_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000027_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000031_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000042_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000043_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000045_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000057_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000067_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000070_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000072_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000108_1.digi",
                                              "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00218171/0000/00218171_00000118_1.digi",
                                             ]
                    
                              },
                
                "KSToMuMu": {"pp_nu7.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000010_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000025_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000026_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000030_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000038_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000054_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000060_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000065_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000068_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000070_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000076_1.digi",
                                          "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000078_1.digi",
                                         ]
                            },
                
                "minbias": {"pp_nu1.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000067_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000120_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000125_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000199_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000299_1.digi",
                                         "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000051_1.digi",
                                         "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000072_1.digi",
                                         "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000123_1.digi",
                                         "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/DIGI/00208776/0000/00208776_00000130_1.digi",
                                        ],
                            
                            "pp_nu7.6": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00001262_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00001962_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00002016_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00002322_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00003659_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00004214_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00004224_1.digi",
                                         "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204942/0000/00204942_00005039_1.digi",
                                        ], 
                            
                            "PbPb_peripheral": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000019_1.xdigi",
                                                "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000028_1.xdigi",
                                                "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000043_1.xdigi",
                                                "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000101_1.xdigi",
                                                "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000132_1.xdigi",
                                                "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000138_1.xdigi",
                                                "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000149_1.xdigi",
                                                "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000185_1.xdigi",
                                               ],
                            
                            "PbPb_central": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000005_1.xdigi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000007_1.xdigi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000008_1.xdigi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000012_1.xdigi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000014_1.xdigi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000020_1.xdigi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000022_1.xdigi",
                                             "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206139/0000/00206139_00000024_1.xdigi",
                                            ],
                            }
               }

# Configurables
evts = -1
col_type  = "pp"
col_stype = "nu1.6"
mc_decay  = "JpsiToMuMu"

options.evt_max    = evts
options.simulation = True
options.data_type  = "Upgrade"
options.print_freq = 1000

options.input_files = (input_files[mc_decay])[f"{col_type}_{col_stype}"]
options.input_type  = 'ROOT'

options.conddb_tag = conddb_tags[col_type]
options.dddb_tag   = dddb_tags[col_type]
options.geometry_version   = 'run3/trunk'
options.conditions_version = 'master'
options.root_ioalg_name = 'RootIOAlgExt'
options.root_ioalg_opts = {'IgnorePaths': ['/Event/Rec/Summary']}

options.output_type = 'ROOT'

if evts != -1:
    options.output_file = f"/eos/lhcb/user/j/jcambonb/private/run3_rta/dst_files/hlt2_VUM_lines_{mc_decay}_{evts}_{col_type}_{col_stype}.dst"
    options.output_manifest_file = f"./manifest_files/hlt2_VUM_lines_{mc_decay}_{evts}_{col_type}_{col_stype}.tck.json"
else:
    options.output_file = f"/eos/lhcb/user/j/jcambonb/private/run3_rta/dst_files/hlt2_VUM_lines_{mc_decay}_{col_type}_{col_stype}.dst"
    options.output_manifest_file = f"./manifest_files/hlt2_VUM_lines_{mc_decay}_{col_type}_{col_stype}.tck.json"
    
# Trigger lines definition

## Tag filter
@configurable
def make_tag_filter(particles,
                    pvs,
                    get_cuts,
                    probe_charge,
                    name='Hlt2TrackEff_TagTrack_{hash}'):
    code = F.require_all(
        (F.CHARGE < 0) if probe_charge == 1 else (F.CHARGE > 0),
        F.CHI2DOF < get_cuts['max_TagTrChi2'],
        F.P > get_cuts['min_TagP'],
        F.PT > get_cuts['min_TagPT'],
        F.ISMUON(),
        F.PID_MU > get_cuts['min_TagMuonID'],
        F.GHOSTPROB() < get_cuts['max_TagGhostProb'],
    )
    if get_cuts['min_TagIP'] is not None:
        code = F.require_all(code, F.MINIP(pvs) > get_cuts['min_TagIP'])
    if get_cuts['min_TagIPchi2'] is not None:
        code = F.require_all(code,
                             F.MINIPCHI2(pvs) > get_cuts['min_TagIPchi2'])
    return ParticleFilter(particles, F.FILTER(code), name=name)

# Probe filter
@configurable
def make_probe_filter(particles,
                      pvs,
                      get_cuts,
                      probe_charge,
                      name='Hlt2TrackEff_ProbeTrack_{hash}'):
    code = F.require_all(
        (F.CHARGE > 0) if probe_charge == 1 else (F.CHARGE < 0),
        F.CHI2DOF < get_cuts['max_ProbeTrChi2'],
        F.P > get_cuts['min_ProbeP'],
        F.PT > get_cuts['min_ProbePT'],
        F.MINIPCHI2(pvs) > get_cuts['min_ProbeIPchi2'],
    )
    if get_cuts['require_ismuon']:
        code = F.require_all(code, F.ISMUON())
    return ParticleFilter(particles, F.FILTER(code), name=name)

# Long filter
@configurable
def make_LongFilter(particles,
                    probe_charge=0,
                    name="Hlt2TrackEff_LongTrack_{hash}"):
    code = (F.CHARGE > 0) if probe_charge == 1 else (F.CHARGE < 0)
    return ParticleFilter(particles, F.FILTER(code), name=name)

@configurable
def make_dimuon_combiner(particles,
                         pvs,
                         get_cuts,
                         probe_charge=0,
                         name='Hlt2_dimuon_combiner_{hash}'):
    #upper JPsi DOCA
    combination_cut = F.require_all(
        in_range(get_cuts['min_Mother_mass'], F.MASS, get_cuts['max_Mother_mass']),
        F.SUM(F.PT) > get_cuts['min_Mother_PT'],
    )
    if get_cuts['max_Mother_DOCA'] is not None:
        combination_cut = F.require_all(combination_cut,
                                        F.MAXDOCACUT(get_cuts['max_Mother_DOCA']))
    #additional cuts
    mother_cut = F.require_all(
        in_range(get_cuts['min_Mother_mass'], F.MASS, get_cuts['max_Mother_mass']),
        F.PT > get_cuts['min_Mother_PT'],
        F.BPVLTIME(pvs) > get_cuts['min_Mother_tau'],
    )
    if get_cuts['max_Mother_VtxChi2'] is not None:
        mother_cut = F.require_all(mother_cut,
                                   F.CHI2DOF < get_cuts['max_Mother_VtxChi2'])
    if get_cuts['max_Mother_IP'] is not None:
        mother_cut = F.require_all(mother_cut,
                                   F.MINIP(pvs) < get_cuts['max_Mother_IP'])
        
    decay_descriptor = "J/psi(1S) -> mu- mu+" if probe_charge == 1 else "J/psi(1S) -> mu+ mu-"
    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cut,
        CompositeCut=mother_cut,
        name=name)

# charmonium -> mu mu  lines
#-------------------------------------------------------------------------------------------------------------------------------
charmonium_cuts = {'max_ProbeTrChi2': 5,
                   'min_ProbeP': 5 * GeV,
                   'min_ProbePT': 0.5 * GeV,
                   'min_ProbeIPchi2': 0,
                   'max_TagTrChi2': 3,
                   'min_TagP': 7 * GeV,
                   'min_TagPT': 0.5 * GeV,
                   'min_TagMuonID': -1,
                   'min_TagIP': 0.2 * mm,
                   'min_TagIPchi2': None,
                   'max_TagGhostProb': 1,
                   'max_Mother_DOCA': 0.1 * mm,
                   'max_Mother_IP': None,
                   'max_Mother_VtxChi2': 20,
                   'min_Mother_PT': 0.2 * GeV,
                   'min_Mother_mass': 2500 * MeV,
                   'max_Mother_mass': 4300 * MeV,
                   'min_Mother_tau': 0 * picosecond,
                   'checkVP': True,
                   'checkFT': False,
                   'checkUT': True,
                   'checkMuon': True,
                   'require_ismuon': False,
                   }

@configurable
def charmonium_Long_dimuons_line(name=f"Hlt2_ccbarToMuMu_Long", 
                                 prescale=1, 
                                 get_cuts=charmonium_cuts):
    
    pvs = make_pvs()
    
    muons = make_ismuon_long_muon()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)
    
@configurable
def charmonium_VUM_dimuons_line(name=f"Hlt2_ccbarToMuMu_VUM", 
                                prescale=1, 
                                get_cuts=charmonium_cuts): 
    pvs = make_pvs()
    
    muons = make_veloutmuon_muons()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)

@configurable
def charmonium_VM_dimuons_line(name=f"Hlt2_ccbarToMuMu_VM", 
                               prescale=1, 
                               get_cuts=charmonium_cuts):
    pvs = make_pvs()
    
    muons = make_velomuon_muons()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)

## Tracking efficiency lines for charmonium -> mu mu for VUM and VM
@configurable
def charmonium_VUM_Tagging_dimuons_line(name,
                                        probe_charge, 
                                        prescale=1, 
                                        get_cuts=charmonium_cuts):
    pvs = make_pvs()
    
    probe_muons = make_probe_filter(make_veloutmuon_muons(), pvs, get_cuts, probe_charge=probe_charge)
    tag_muons = make_tag_filter(make_ismuon_long_muon(), pvs, get_cuts, probe_charge=probe_charge)
    
    particles = [tag_muons, probe_muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts, probe_charge=probe_charge)
    return Hlt2Line(name=name, 
                    algs=make_prefilters() + [tag_muons, probe_muons, dimuon_comb], 
                    prescale=prescale)

@configurable
def charmonium_VUM_Matching_dimuons_line(name,
                                         probe_charge, 
                                         prescale=1, 
                                         get_cuts=charmonium_cuts):
    pvs = make_pvs()
    
    probe_muons = make_probe_filter(make_veloutmuon_muons(), pvs, get_cuts, probe_charge=probe_charge)
    tag_muons   = make_tag_filter(make_ismuon_long_muon(), pvs, get_cuts, probe_charge=probe_charge)
    long_track  = make_LongFilter(make_ismuon_long_muon(), probe_charge=probe_charge)
    
    particles = [tag_muons, probe_muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts, probe_charge=probe_charge)
    
    matcher = MuonProbeToLongMatcher(
        TwoBodyComposites=dimuon_comb,
        LongTracks=long_track,
        checkVP=get_cuts['checkVP'],
        checkFT=get_cuts['checkFT'],
        checkUT=get_cuts['checkUT'],
        checkMuon=get_cuts['checkMuon'])
    dimuon_matched = matcher.MatchedComposites
    long_matched = matcher.MatchedLongTracks
    long_save = make_LongFilter(long_matched, probe_charge=probe_charge)
    
    return Hlt2Line(name=name, 
                    algs=make_prefilters() + [tag_muons, probe_muons, dimuon_comb, dimuon_matched],
                    extra_outputs=[("LongMatched", long_save)],
                    prescale=prescale)
@configurable
def charmonium_VM_Tagging_dimuons_line(name,
                                       probe_charge, 
                                       prescale=1, 
                                       get_cuts=charmonium_cuts):
    pvs = make_pvs()
    
    probe_muons = make_probe_filter(make_velomuon_muons(), pvs, get_cuts, probe_charge=probe_charge)
    tag_muons = make_tag_filter(make_ismuon_long_muon(), pvs, get_cuts, probe_charge=probe_charge)
    
    particles = [tag_muons, probe_muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts, probe_charge=probe_charge)
    return Hlt2Line(name=name, 
                    algs=make_prefilters() + [tag_muons, probe_muons, dimuon_comb], 
                    prescale=prescale)

@configurable
def charmonium_VM_Matching_dimuons_line(name,
                                        probe_charge, 
                                        prescale=1, 
                                        get_cuts=charmonium_cuts):
    pvs = make_pvs()
    
    probe_muons = make_probe_filter(make_velomuon_muons(), pvs, get_cuts, probe_charge=probe_charge)
    tag_muons = make_tag_filter(make_ismuon_long_muon(), pvs, get_cuts, probe_charge=probe_charge)
    long_track = make_LongFilter(make_ismuon_long_muon(), probe_charge=probe_charge)
    
    particles = [tag_muons, probe_muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts, probe_charge=probe_charge)
    
    matcher = MuonProbeToLongMatcher(
        TwoBodyComposites=dimuon_comb,
        LongTracks=long_track,
        checkVP=get_cuts['checkVP'],
        checkFT=get_cuts['checkFT'],
        checkUT=False,
        checkMuon=get_cuts['checkMuon'])
    dimuon_matched = matcher.MatchedComposites
    long_matched = matcher.MatchedLongTracks
    long_save = make_LongFilter(long_matched, probe_charge=probe_charge)
    
    return Hlt2Line(name=name, 
                    algs=make_prefilters() + [tag_muons, probe_muons, dimuon_comb, dimuon_matched],
                    extra_outputs=[("LongMatched", long_save)],
                    prescale=prescale)
#-------------------------------------------------------------------------------------------------------------------------------

# bottomonium -> mu mu  lines
#-------------------------------------------------------------------------------------------------------------------------------
bottomonium_cuts = {'max_ProbeTrChi2': 5,
                    'min_ProbeP': 5 * GeV,
                    'min_ProbePT': 0.5 * GeV,
                    'min_ProbeIPchi2': 0,
                    'max_TagTrChi2': 3,
                    'min_TagP': 7 * GeV,
                    'min_TagPT': 0.5 * GeV,
                    'min_TagMuonID': -1,
                    'min_TagIP': 0.2 * mm,
                    'min_TagIPchi2': None,
                    'max_TagGhostProb': 1,
                    'max_Mother_DOCA': 0.1 * mm,
                    'max_Mother_IP': None,
                    'max_Mother_VtxChi2': 20,
                    'min_Mother_PT': 0.2 * GeV,
                    'min_Mother_mass': 8000 * MeV,
                    'max_Mother_mass': 13000 * MeV,
                    'min_Mother_tau': 0 * picosecond,
                    'checkVP': True,
                    'checkFT': False,
                    'checkUT': False,
                    'checkMuon': True,
                    'require_ismuon': False,
                   }

@configurable
def bottomonium_Long_dimuons_line(name=f"Hlt2_bbbarToMuMu_Long", 
                                  prescale=1, 
                                  get_cuts=bottomonium_cuts):
    pvs = make_pvs()
    
    muons = make_ismuon_long_muon()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)
    
@configurable
def bottomonium_VUM_dimuons_line(name=f"Hlt2_bbbarToMuMu_VUM", 
                                 prescale=1, 
                                 get_cuts=bottomonium_cuts):
    pvs = make_pvs()
    
    muons = make_veloutmuon_muons()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)

@configurable
def bottomonium_VM_dimuons_line(name=f"Hlt2_bbbarToMuMu_VM", 
                                prescale=1, 
                                get_cuts=bottomonium_cuts):
    pvs = make_pvs()
    
    muons = make_velomuon_muons()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)
#-------------------------------------------------------------------------------------------------------------------------------

# low mass dimuon -> mu mu  lines
#-------------------------------------------------------------------------------------------------------------------------------
low_mass_cuts = {'max_ProbeTrChi2': 5,
                 'min_ProbeP': 5 * GeV,
                 'min_ProbePT': 0.5 * GeV,
                 'min_ProbeIPchi2': 0,
                 'max_TagTrChi2': 3,
                 'min_TagP': 7 * GeV,
                 'min_TagPT': 0.5 * GeV,
                 'min_TagMuonID': -1,
                 'min_TagIP': 0.2 * mm,
                 'min_TagIPchi2': None,
                 'max_TagGhostProb': 1,
                 'max_Mother_DOCA': 0.1 * mm,
                 'max_Mother_IP': None,
                 'max_Mother_VtxChi2': 20,
                 'min_Mother_PT': 0.2 * GeV,
                 'min_Mother_mass': 0 * MeV,
                 'max_Mother_mass': 1000 * MeV,
                 'min_Mother_tau': 0 * picosecond,
                 'checkVP': True,
                 'checkFT': False,
                 'checkUT': True,
                 'checkMuon': True,
                 'require_ismuon': False,
                }

@configurable
def low_mass_Long_dimuons_line(name=f"Hlt2_lowmassToMuMu_Long", 
                               prescale=1, 
                               get_cuts=low_mass_cuts):
    pvs = make_pvs()
    
    muons = make_ismuon_long_muon()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)
    
@configurable
def low_mass_VUM_dimuons_line(name=f"Hlt2_lowmassToMuMu_VUM", 
                              prescale=1, 
                              get_cuts=low_mass_cuts):
    pvs = make_pvs()
    
    muons = make_veloutmuon_muons()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)

@configurable
def low_mass_VM_dimuons_line(name=f"Hlt2_lowmassToMuMu_VM", 
                             prescale=1, 
                             get_cuts=low_mass_cuts):
    pvs = make_pvs()
    
    muons = make_velomuon_muons()
    particles = [muons, muons]
    dimuon_comb = make_dimuon_combiner(particles, pvs, get_cuts)
    return Hlt2Line(name=name, algs=make_prefilters() + [dimuon_comb], prescale=prescale)
#-------------------------------------------------------------------------------------------------------------------------------

# Running Moore
def make_lines():
    
    if "Jpsi" or "Psi2S" in mc_decay:
        
        line_head = "Hlt2_ccbarToMuMu"

        dimuon_lines = [charmonium_Long_dimuons_line(), charmonium_VUM_dimuons_line(), charmonium_VM_dimuons_line()]
        
        tagging_VUM_lines  = [charmonium_VUM_Tagging_dimuons_line(name=f"{line_head}_VUM_{muon}_Tag", probe_charge=ch) 
                              for muon, ch in zip(['mup', 'mum'], [1, 0])]
        matching_VUM_lines = [charmonium_VUM_Matching_dimuons_line(name=f"{line_head}_VUM_{muon}_Match", probe_charge=ch)
                              for muon, ch in zip(['mup', 'mum'], [1, 0])]
        
        tagging_VM_lines  = [charmonium_VM_Tagging_dimuons_line(name=f"{line_head}_VM_{muon}_Tag", probe_charge=ch) 
                             for muon, ch in zip(['mup', 'mum'], [1, 0])]
        matching_VM_lines = [charmonium_VM_Matching_dimuons_line(name=f"{line_head}_VM_{muon}_Match", probe_charge=ch) 
                             for muon, ch in zip(['mup', 'mum'], [1, 0])]

        
        lines = dimuon_lines + tagging_VM_lines + tagging_VUM_lines + matching_VM_lines + matching_VUM_lines
        
        
    if "Upsi" in mc_decay:
        dimuon_lines = [bottomonium_Long_dimuons_line(), bottomonium_VUM_dimuons_line(), bottomonium_VM_dimuons_line()]
        
        lines = dimuon_lines
        
    if "KS" in mc_decay or mc_decay == "minbias":
        dimuon_lines = [low_mass_Long_dimuons_line(), low_mass_VUM_dimuons_line(), low_mass_VM_dimuons_line()]

        lines = dimuon_lines
        
    return lines

public_tools = []

if col_type == "pp":
    with reco.bind(from_file=False),\
        make_fastest_reconstruction.bind(skipUT=False),\
        reconstruction.bind(make_reconstruction=make_fastest_reconstruction):
        run_moore(options, make_lines)

if col_type == "PbPb":
    with reco.bind(from_file=False),\
        make_fastest_reconstruction.bind(skipUT=False),\
        require_gec.bind(cut=30000),\
        PrHybridSeeding.bind(RemoveBeamHole=True, RemoveClones_forLead=True),\
        reconstruction.bind(make_reconstruction=make_fastest_reconstruction),\
        make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
        make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
        persistreco_writing_version.bind(version=1.1),\
        make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.5):
        config = run_moore(options, make_lines, public_tools)