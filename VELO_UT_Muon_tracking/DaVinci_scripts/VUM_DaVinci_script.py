from PyConf.reading import get_particles, get_pvs, get_rec_summary
from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection 
import FunTuple.functorcollections as FC
import Functors as F
from DaVinciMCTools import MCTruthAndBkgCat

def charmonium_tuples(options: Options):
    line_head = "Hlt2_ccbarToMuMu"
    
    track_types = ["Long", "VUM", "VM"] 
    track_tags  = ["VUM_mum_Tag", "VUM_mup_Tag", "VM_mum_Tag", "VM_mup_Tag"]
    track_match = ["VUM_mum_Match", "VUM_mup_Match", "VM_mum_Match", "VM_mup_Match"]
    
    names = track_types + track_tags + track_match
    
    lines = [f"{line_head}_{name}" for name in names]
    
    datas = [get_particles(f"/Event/HLT2/{line}/Particles") for line in lines]
    prefilters = [create_lines_filter(name=f"PreFilter_{line}", lines=[line]) for line in lines]
    
    pvs = get_pvs()
    rec_summary = get_rec_summary()


    fields = {"dimuon": "J/psi(1S) -> mu+ mu-",
              "mup"   : "J/psi(1S) -> ^mu+ mu-",
              "mum"   : "J/psi(1S) -> mu+ ^mu-"}
    
    # Functors for all fields
    global_features = FC.Kinematics()
    
    global_features += FunctorCollection({"ETA"      : F.ETA,
                                          "PHI"      : F.PHI,
                                          "CHI2"     : F.CHI2,
                                          "CHI2DOF"  : F.CHI2DOF,
                                          "BPVX"     : F.BPVX(pvs),
                                          "BPVY"     : F.BPVY(pvs),
                                          "BPVZ"     : F.BPVZ(pvs),
                                          "BPVIP"    : F.BPVIP(pvs),
                                          "BPVIPCHI2": F.BPVIPCHI2(pvs),
                                          "CHARGE"   : F.CHARGE,
                                          "ISBASIC"  : F.ISBASICPARTICLE,
                                          "HASTRACK" : F.HAS_VALUE @ F.TRACK,
                                          })
    
    # Functors for head of the decay
    
    # Functors for daughters
    track_features = FC.ParticleID(extra_info=True)

    track_features += FunctorCollection({"GHOSTPROB": F.GHOSTPROB,
                                         "NHITS"    : F.VALUE_OR(-1) @ F.NHITS @ F.TRACK,
                                         "NVPHITS"  : F.VALUE_OR(-1) @ F.NVPHITS @ F.TRACK,
                                         "NFTHITS"  : F.VALUE_OR(-1) @ F.NFTHITS @ F.TRACK,
                                         "QOVERP"   : F.QOVERP @ F.TRACK,
                                         "TRACK_P"  : F.TRACK_MOMVEC,
                                         "TrP"      : F.TRACK_P,
                                         })
    # Functors for event info
    evt_info = FC.EventInfo()

    evt_info += FunctorCollection({'nTracks'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nTracks'),
                                   'nPVs'              : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nPVs"),
                                   'nFTClusters'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nFTClusters"),
                                   'nVPClusters'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nVPClusters'),
                                   'nEcalClusters'     : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nEcalClusters'),
                                   'eCalTot'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'eCalTot'),
                                   'hCalTot'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'hCalTot'),
                                   'nVeloTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nVeloTracks'),
                                   'nLongTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nLongTracks'),
                                   'nDownstreamTracks' : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nDownstreamTracks'),
                                   'nUpstreamTracks'   : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nUpstreamTracks'),
                                   'nBackTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nBackTracks'),
                                   'nRich1Hits'        : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nRich1Hits'),
                                   'nRich2Hits'        : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nRich2Hits'),
                                   "ALLPVX"            : F.ALLPVX(pvs),
                                   "ALLPVY"            : F.ALLPVY(pvs),
                                   "ALLPVZ"            : F.ALLPVZ(pvs)
                                   })
      
    # MCTruth functors
    if options.simulation == True:
        mc_gb_features_per_line = []
        mc_hd_features_per_line = []
        
        for data, name in zip(datas, names):
            mc_gb_features = FunctorCollection({})
            mc_hd_features = FunctorCollection({})
            
            mctruth = MCTruthAndBkgCat(input_particles=data, 
                                       name=f"MCTruthAndBkgCat_functor_{name}")

            mc_gb_features += FC.MCKinematics(mctruth_alg=mctruth)
            mc_gb_features += FC.MCHierarchy(mctruth_alg=mctruth)
            mc_gb_features += FC.MCPrimaryVertexInfo(mctruth_alg=mctruth)
            mc_hd_features.update({"BKGCAT": mctruth.BkgCat})
            mc_hd_features += FC.MCPromptDecay(mctruth_alg=mctruth)

            mc_gb_features_per_line.append(mc_gb_features)
            mc_hd_features_per_line.append(mc_hd_features)
    
        features = [{"ALL"   : global_features + mc_gb_features,
                     "dimuon": mc_hd_features,
                     "mum"   : track_features,
                     "mup"   : track_features,}
                     for mc_gb_features, mc_hd_features in zip(mc_gb_features_per_line, mc_hd_features_per_line)]
    else:
        features = {"ALL"   : global_features,
                    "dimuon": mc_hd_features,
                    "mum"   : track_features,
                    "mup"   : track_features,}
        
    dtts = [Funtuple(name=f"charmonium_mumu_{name}",
                     tuple_name="DecayTree",
                     fields=fields,
                     variables=features,
                     inputs=data,
                     event_variables=evt_info) for name, data, features in zip(names, datas, features)]
    
    algs = {line: [prefilt, dtt] for line, dtt, prefilt in zip(lines, dtts, prefilters)}
    
    return make_config(options, algs)


def bottomonium_tuples(options: Options):
    line_head = "Hlt2_bbbarToMuMu"
    track_types = ["Long", "VUM", "VM"]
    
    lines  = [f"{line_head}_{track}" for track in track_types]
    
    datas = [get_particles(f"/Event/HLT2/{line}/Particles") for line in lines]
    prefilters = [create_lines_filter(name=f"PreFilter_{line}", lines=[line]) for line in lines]
    
    pvs = get_pvs()
    rec_summary = get_rec_summary()


    fields = {"dimuon": "J/psi(1S) -> mu+ mu-",
              "mup"   : "J/psi(1S) -> ^mu+ mu-",
              "mum"   : "J/psi(1S) -> mu+ ^mu-"}
    
    # Functors for all fields
    global_features = FC.Kinematics()
    
    global_features += FunctorCollection({"ETA"      : F.ETA,
                                          "PHI"      : F.PHI,
                                          "CHI2"     : F.CHI2,
                                          "CHI2DOF"  : F.CHI2DOF,
                                          "BPVX"     : F.BPVX(pvs),
                                          "BPVY"     : F.BPVY(pvs),
                                          "BPVZ"     : F.BPVZ(pvs),
                                          "BPVIP"    : F.BPVIP(pvs),
                                          "BPVIPCHI2": F.BPVIPCHI2(pvs),
                                          "CHARGE"   : F.CHARGE,
                                          "ISBASIC"  : F.ISBASICPARTICLE,
                                          "HASTRACK" : F.HAS_VALUE @ F.TRACK,
                                          })
    
    # Functors for head of the decay
    
    # Functors for daughters
    track_features = FC.ParticleID(extra_info=True)

    track_features += FunctorCollection({"GHOSTPROB": F.GHOSTPROB,
                                         "NHITS"    : F.VALUE_OR(-1) @ F.NHITS @ F.TRACK,
                                         "NVPHITS"  : F.VALUE_OR(-1) @ F.NVPHITS @ F.TRACK,
                                         "NFTHITS"  : F.VALUE_OR(-1) @ F.NFTHITS @ F.TRACK,
                                         "QOVERP"   : F.QOVERP @ F.TRACK,
                                         "TRACK_P"  : F.TRACK_MOMVEC,
                                         "TrP"      : F.TRACK_P,
                                         })
    # Functors for event info
    evt_info = FC.EventInfo()

    evt_info += FunctorCollection({'nTracks'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nTracks'),
                                   'nPVs'              : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nPVs"),
                                   'nFTClusters'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nFTClusters"),
                                   'nVPClusters'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nVPClusters'),
                                   'nEcalClusters'     : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nEcalClusters'),
                                   'eCalTot'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'eCalTot'),
                                   'hCalTot'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'hCalTot'),
                                   'nVeloTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nVeloTracks'),
                                   'nLongTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nLongTracks'),
                                   'nDownstreamTracks' : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nDownstreamTracks'),
                                   'nUpstreamTracks'   : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nUpstreamTracks'),
                                   'nBackTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nBackTracks'),
                                   'nRich1Hits'        : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nRich1Hits'),
                                   'nRich2Hits'        : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nRich2Hits'),
                                   "ALLPVX"            : F.ALLPVX(pvs),
                                   "ALLPVY"            : F.ALLPVY(pvs),
                                   "ALLPVZ"            : F.ALLPVZ(pvs)
                                   })
      
    # MCTruth functors
    if options.simulation == True:
        mc_gb_features_per_line = []
        mc_hd_features_per_line = []
        
        for data, track in zip(datas, track_types):
            mc_gb_features = FunctorCollection({})
            mc_hd_features = FunctorCollection({})
            
            mctruth = MCTruthAndBkgCat(input_particles=data, 
                                       name=f"MCTruthAndBkgCat_functor_{track}")

            mc_gb_features += FC.MCKinematics(mctruth_alg=mctruth)
            mc_gb_features += FC.MCHierarchy(mctruth_alg=mctruth)
            mc_gb_features += FC.MCPrimaryVertexInfo(mctruth_alg=mctruth)
            mc_hd_features.update({"BKGCAT": mctruth.BkgCat})
            mc_hd_features += FC.MCPromptDecay(mctruth_alg=mctruth)

            mc_gb_features_per_line.append(mc_gb_features)
            mc_hd_features_per_line.append(mc_hd_features)
    
        features = [{"ALL"   : global_features + mc_gb_features,
                     "dimuon": mc_hd_features,
                     "mum"   : track_features,
                     "mup"   : track_features,}
                     for mc_gb_features, mc_hd_features in zip(mc_gb_features_per_line, mc_hd_features_per_line)]
    else:
        features = {"ALL"   : global_features,
                    "dimuon": mc_hd_features,
                    "mum"   : track_features,
                    "mup"   : track_features,}
        
    dtts = [Funtuple(name=f"bottomonium_mumu_{track}",
                     tuple_name="DecayTree",
                     fields=fields,
                     variables=features,
                     inputs=data,
                     event_variables=evt_info) for track, data, features in zip(track_types, datas, features)]
    
    algs = {line: [prefilt, dtt] for line, dtt, prefilt in zip(lines, dtts, prefilters)}
    
    return make_config(options, algs)


def low_mass_dimuon_tuples(options: Options):
    line_head = "Hlt2_lowmassToMuMu"
    track_types = ["Long", "VUM", "VM"]
    
    lines = [f"{line_head}_{track}" for track in track_types]
    
    datas = [get_particles(f"/Event/HLT2/{line}/Particles") for line in lines]
    prefilters = [create_lines_filter(name=f"PreFilter_{line}", lines=[line]) for line in lines]
    
    pvs = get_pvs()
    rec_summary = get_rec_summary()


    fields = {"dimuon": "J/psi(1S) -> mu+ mu-",
              "mup"   : "J/psi(1S) -> ^mu+ mu-",
              "mum"   : "J/psi(1S) -> mu+ ^mu-"}
    
    # Functors for all fields
    global_features = FC.Kinematics()
    
    global_features += FunctorCollection({"ETA"      : F.ETA,
                                          "PHI"      : F.PHI,
                                          "CHI2"     : F.CHI2,
                                          "CHI2DOF"  : F.CHI2DOF,
                                          "BPVX"     : F.BPVX(pvs),
                                          "BPVY"     : F.BPVY(pvs),
                                          "BPVZ"     : F.BPVZ(pvs),
                                          "BPVIP"    : F.BPVIP(pvs),
                                          "BPVIPCHI2": F.BPVIPCHI2(pvs),
                                          "CHARGE"   : F.CHARGE,
                                          "ISBASIC"  : F.ISBASICPARTICLE,
                                          "HASTRACK" : F.HAS_VALUE @ F.TRACK,
                                          })
    
    # Functors for head of the decay
    
    # Functors for daughters
    track_features = FC.ParticleID(extra_info=True)

    track_features += FunctorCollection({"GHOSTPROB": F.GHOSTPROB,
                                         "NHITS"    : F.VALUE_OR(-1) @ F.NHITS @ F.TRACK,
                                         "NVPHITS"  : F.VALUE_OR(-1) @ F.NVPHITS @ F.TRACK,
                                         "NFTHITS"  : F.VALUE_OR(-1) @ F.NFTHITS @ F.TRACK,
                                         "QOVERP"   : F.QOVERP @ F.TRACK,
                                         "TRACK_P"  : F.TRACK_MOMVEC,
                                         "TrP"      : F.TRACK_P,
                                         })
    # Functors for event info
    evt_info = FC.EventInfo()

    evt_info += FunctorCollection({'nTracks'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nTracks'),
                                   'nPVs'              : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nPVs"),
                                   'nFTClusters'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nFTClusters"),
                                   'nVPClusters'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nVPClusters'),
                                   'nEcalClusters'     : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nEcalClusters'),
                                   'eCalTot'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'eCalTot'),
                                   'hCalTot'           : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'hCalTot'),
                                   'nVeloTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nVeloTracks'),
                                   'nLongTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nLongTracks'),
                                   'nDownstreamTracks' : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nDownstreamTracks'),
                                   'nUpstreamTracks'   : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nUpstreamTracks'),
                                   'nBackTracks'       : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nBackTracks'),
                                   'nRich1Hits'        : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nRich1Hits'),
                                   'nRich2Hits'        : F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, 'nRich2Hits'),
                                   "ALLPVX"            : F.ALLPVX(pvs),
                                   "ALLPVY"            : F.ALLPVY(pvs),
                                   "ALLPVZ"            : F.ALLPVZ(pvs)
                                   })
      
    # MCTruth functors
    if options.simulation == True:
        mc_gb_features_per_line = []
        mc_hd_features_per_line = []
        
        for data, track in zip(datas, track_types):
            mc_gb_features = FunctorCollection({})
            mc_hd_features = FunctorCollection({})
            
            mctruth = MCTruthAndBkgCat(input_particles=data, 
                                       name=f"MCTruthAndBkgCat_functor_{track}")

            mc_gb_features += FC.MCKinematics(mctruth_alg=mctruth)
            mc_gb_features += FC.MCHierarchy(mctruth_alg=mctruth)
            mc_gb_features += FC.MCPrimaryVertexInfo(mctruth_alg=mctruth)
            mc_hd_features.update({"BKGCAT": mctruth.BkgCat})
            mc_hd_features += FC.MCPromptDecay(mctruth_alg=mctruth)

            mc_gb_features_per_line.append(mc_gb_features)
            mc_hd_features_per_line.append(mc_hd_features)
    
        features = [{"ALL"   : global_features + mc_gb_features,
                     "dimuon": mc_hd_features,
                     "mum"   : track_features,
                     "mup"   : track_features,}
                     for mc_gb_features, mc_hd_features in zip(mc_gb_features_per_line, mc_hd_features_per_line)]
    else:
        features = {"ALL"   : global_features,
                    "dimuon": mc_hd_features,
                    "mum"   : track_features,
                    "mup"   : track_features,}
        
    dtts = [Funtuple(name=f"low_mass_dimuon_mumu_{track}",
                     tuple_name="DecayTree",
                     fields=fields,
                     variables=features,
                     inputs=data,
                     event_variables=evt_info) for track, data, features in zip(track_types, datas, features)]
    
    algs = {line: [prefilt, dtt] for line, dtt, prefilt in zip(lines, dtts, prefilters)}
    
    return make_config(options, algs)