import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import mplhep
import sys 
import os

mplhep.styles.use(mplhep.styles.LHCb2)

# Data reading

pp_colls = ["pp7.6"]

path = "../DaVinci_scripts/root_files"

files_ks_pp = [f"KS_mumu_{col}_exp24_tuple.root" for col in pp_colls]

muon_types = ["Long", "VUM", "VM"]

dtts  = [f"low_mass_dimuon_mumu_{muon}"  for muon in muon_types]

tdfs_ks_pp  = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
               for colls, files in zip(pp_colls, files_ks_pp)}

tdfs_ks_pp = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_ks_pp.items()}
tdfs_ks_pp = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_ks_pp.items()}

columns = ["mup_P", "mum_P", 
           "mup_MC_MOTHER_ID", "mum_MC_MOTHER_ID", 
           "mup_P_Res", "mum_P_Res"]

dfs_ks_pp = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
             for colls, tdfs in tdfs_ks_pp.items()}

ks_truth = "mup_MC_MOTHER_ID == 310 & mum_MC_MOTHER_ID == 310"

colors = ["black", "red", "blue"]

print("Total number of signal candidates")

for colls1, dfs1 in dfs_ks_pp.items():
    
    print(f"KS to mu mu for {colls1}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs1):
        print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    ("#----------------------------------")
    print("#----------------------------------")


# pp feature plots
#------------------------------------------------------------------------------------------------------------------
i = 1; j = 100; k = 1000; t = 10000; l = 100000

for colls_ks, dfs_ks in dfs_ks_pp.items():
    plt.figure(i)
    for df_ks, muons, color in zip(dfs_ks, muon_types, colors):
        plt.hist((df_ks.query(ks_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_ks}  MagDown exp24")
    plt.savefig(f"./plots/ks_mass_{colls_ks}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(l)
    plt.subplot(1, 2, 1)
    for df_ks, muons, color in zip(dfs_ks, muon_types, colors):
        plt.hist((df_ks.query(ks_truth))["mup_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^+)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.subplot(1, 2, 2)
    for df_ks, muons, color in zip(dfs_ks, muon_types, colors):
        plt.hist((df_ks.query(ks_truth))["mup_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^-)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.suptitle(f"{colls_ks}  MagDown exp24")
    plt.savefig(f"./plots/ks_muon_momentum_reso_{colls_ks}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    i += 1; j += 1; k += 1; t += 1

#------------------------------------------------------------------------------------------------------------------
