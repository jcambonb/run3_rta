import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import mplhep
import sys 
import os

mplhep.styles.use(mplhep.styles.LHCb2)

pp_colls   = ["pp1.6"]
#PbPb_colls = ["PbPb_peripheral", "PbPb_peripheral"]

path = "../DaVinci_scripts/root_files"

files_jpsi_pp   = [f"Jpsi_mumu_{col}_exp24_tuple.root" for col in pp_colls]
#files_jpsi_PbPb = [f"Jpsi_mumu_{col}_100_exp24_tuple.root" for col in PbPb_colls]

track_tags  = ["VUM_mum_Tag", "VUM_mup_Tag", "VM_mum_Tag", "VM_mup_Tag"]
track_match = ["VUM_mum_Match", "VUM_mup_Match", "VM_mum_Match", "VM_mup_Match"]

muon_types = track_tags + track_match

dtts  = [f"charmonium_mumu_{muon}" for muon in muon_types]

tdfs_jpsi_pp   = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
                  for colls, files in zip(pp_colls, files_jpsi_pp)}
#tdfs_jpsi_PbPb = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
#                  for colls, files in zip(PbPb_colls, files_jpsi_PbPb)}

tdfs_jpsi_pp = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_pp.items()}
tdfs_jpsi_pp = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_pp.items()}

#tdfs_jpsi_PbPb = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_PbPb.items()}
#tdfs_jpsi_PbPb = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_PbPb.items()}

columns = ["mup_P", "mum_P", 
           "mup_MC_MOTHER_ID", "mum_MC_MOTHER_ID", 
           "mup_P_Res", "mum_P_Res"]

dfs_jpsi_pp   = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
                 for colls, tdfs in tdfs_jpsi_pp.items()}
#dfs_jpsi_PbPb = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
#                 for colls, tdfs in tdfs_jpsi_PbPb.items()}

jpsi_truth  = "mup_MC_MOTHER_ID == 443 & mum_MC_MOTHER_ID == 443"

print("Total number of signal candidates")

#for (colls1, dfs1), (colls2, dfs2) in zip(dfs_jpsi_pp.items(), dfs_jpsi_PbPb.items()):
for colls1, dfs1 in dfs_jpsi_pp.items():
    
    print(f"JpsiToMuMu for {colls1}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs1):
        print(f"Signal candidates for {muon} muons = {len((df.query(jpsi_truth))["mup_P"])}")
    ("#----------------------------------")
    
    #print(" ")
    #print(f"Psi2sToMuMu for {colls2}")
    #print("#----------------------------------")
    #for muon, df in zip(muon_types, dfs2):
    #    print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    #print("#----------------------------------")

i = 0; j = 10
for colls_jpsi, dfs_jpsi in dfs_jpsi_pp.items():
    plt.figure(i, figsize=(14, 10))
    for df_jpsi, muons in zip(dfs_jpsi, muon_types):
        if "Tag" in muons:
            if "mup" in muons:
                plt.subplot(1, 2, 1)
                plt.hist((df_jpsi.query(jpsi_truth))["dimuon_M"], bins=100, histtype="step", label=f"{muons.replace("_mup_Tag", "")}+Long")
                plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
                plt.ylabel("A.U")
                plt.legend()

            if "mum" in muons:
                plt.subplot(1, 2, 2)
                plt.hist((df_jpsi.query(jpsi_truth))["dimuon_M"], bins=100, histtype="step", label=f"{muons.replace("_mum_Tag", "")}+Long")     
                plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
                plt.ylabel("A.U")
                plt.legend()
                
    plt.suptitle(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_mass_{colls_jpsi}_trackeff.pdf", dpi=300, bbox_inches='tight')
    plt.show() 
    
    plt.figure(j, figsize=(14, 10))
    for df_jpsi, muons in zip(dfs_jpsi, muon_types):
        if "Tag" in muons:
            if "mup" in muons:
                plt.subplot(1, 2, 1)
                plt.hist((df_jpsi.query(jpsi_truth))["mup_P_Res"], bins=100, histtype="step", label=f"{muons.replace("_mup_Tag", "")}+Long")
                plt.xlabel(r"$\delta p/p(\mu^+)$")                
                plt.ylabel("A.U")
                plt.legend()
                plt.xlim(-0.25, 0.25)

            if "mum" in muons:
                plt.subplot(1, 2, 2)
                plt.hist((df_jpsi.query(jpsi_truth))["mup_P_Res"], bins=100, histtype="step", label=f"{muons.replace("_mum_Tag", "")}+Long")     
                plt.xlabel(r"$\delta p/p(\mu^+)$")                
                plt.ylabel("A.U")
                plt.legend()
                plt.xlim(-0.25, 0.25)
    plt.suptitle(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_mu_p_reso_{colls_jpsi}_trackeff.pdf", dpi=300, bbox_inches='tight')
    plt.show() 
    
    i += 1; j += 1