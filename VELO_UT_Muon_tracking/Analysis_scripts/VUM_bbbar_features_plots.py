import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import mplhep
import sys 
import os

mplhep.styles.use(mplhep.styles.LHCb2)

# Data reading

pp_colls = ["pp7.6"]

path = "../DaVinci_scripts/root_files"

files_y1s_pp = [f"Y1S_mumu_{col}_exp24_tuple.root" for col in pp_colls]
files_y2s_pp = [f"Y2S_mumu_{col}_exp24_tuple.root" for col in pp_colls]
files_y3s_pp = [f"Y3S_mumu_{col}_exp24_tuple.root" for col in pp_colls]

muon_types = ["Long", "VUM", "VM"]

dtts  = [f"bottomonium_mumu_{muon}"  for muon in muon_types]

tdfs_y1s_pp  = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
                for colls, files in zip(pp_colls, files_y1s_pp)}
tdfs_y2s_pp  = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
                for colls, files in zip(pp_colls, files_y2s_pp)}
tdfs_y3s_pp  = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
                for colls, files in zip(pp_colls, files_y3s_pp)}

tdfs_y1s_pp = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_y1s_pp.items()}
tdfs_y1s_pp = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_y1s_pp.items()}

tdfs_y2s_pp = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_y2s_pp.items()}
tdfs_y2s_pp = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_y2s_pp.items()}

tdfs_y3s_pp = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_y3s_pp.items()}
tdfs_y3s_pp = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_y3s_pp.items()}

columns = ["mup_P", "mum_P", 
           "mup_MC_MOTHER_ID", "mum_MC_MOTHER_ID", 
           "mup_P_Res", "mum_P_Res"]

dfs_y1s_pp = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
              for colls, tdfs in tdfs_y1s_pp.items()}
dfs_y2s_pp = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
              for colls, tdfs in tdfs_y2s_pp.items()}
dfs_y3s_pp = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
              for colls, tdfs in tdfs_y3s_pp.items()}

y1s_truth = "mup_MC_MOTHER_ID == 553 & mum_MC_MOTHER_ID == 553"
y2s_truth = "mup_MC_MOTHER_ID == 100553 & mum_MC_MOTHER_ID == 100553"
y3s_truth = "mup_MC_MOTHER_ID == 200553 & mum_MC_MOTHER_ID == 200553"

colors = ["black", "red", "blue"]

print("Total number of signal candidates")

for (colls1, dfs1), (colls2, dfs2), (colls3, dfs3) in zip(dfs_y1s_pp.items(), dfs_y2s_pp.items(), dfs_y3s_pp.items()):
    
    print(f"Y1S to mu mu for {colls1}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs1):
        print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    ("#----------------------------------")
    
    print(" ")
    print(f"Y2S to mu mu  for {colls2}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs2):
        print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    print("#----------------------------------")

    print(" ")
    print(f"Y3S to mu mu  for {colls3}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs3):
        print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    print("#----------------------------------")


# pp feature plots
#------------------------------------------------------------------------------------------------------------------
i = 1; j = 100; k = 1000; t = 10000; l = 100000

for (colls_y1s, dfs_y1s), (colls_y2s, dfs_y2s), (colls_y3s, dfs_y3s) in zip(dfs_y1s_pp.items(), dfs_y2s_pp.items(), dfs_y3s_pp.items()):
    plt.figure(i)
    for df_y1s, muons, color in zip(dfs_y1s, muon_types, colors):
        plt.hist((df_y1s.query(y1s_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_y1s}  MagDown exp24")
    plt.savefig(f"./plots/y1s_mass_{colls_y1s}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(j)
    for df_y2s, muons, color in zip(dfs_y2s, muon_types, colors):
        plt.hist((df_y2s.query(y2s_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_y2s}  MagDown exp24")
    plt.savefig(f"./plots/y2s_mass_{colls_y2s}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(k)
    for df_y3s, muons, color in zip(dfs_y3s, muon_types, colors):
        plt.hist((df_y3s.query(y3s_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_y3s}  MagDown exp24")
    plt.savefig(f"./plots/y3s_mass_{colls_y3s}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(t)
    for df_y1s, df_y2s, df_y3s, muons, color in zip(dfs_y1s, dfs_y2s, dfs_y3s, muon_types, colors):
        plt.hist((df_y1s.query(y1s_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
        plt.hist((df_y2s.query(y2s_truth))["dimuon_M"], bins=100, histtype="step", color=color)
        plt.hist((df_y3s.query(y3s_truth))["dimuon_M"], bins=100, histtype="step", color=color)
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.xlim(8000, 12000)
    plt.title(f"{colls_y1s}  MagDown exp24")
    plt.savefig(f"./plots/bbar_dimuon_mass_{colls_y1s}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(l)
    plt.subplot(1, 2, 1)
    for df_y1s, muons, color in zip(dfs_y1s, muon_types, colors):
        plt.hist((df_y1s.query(y1s_truth))["mup_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^+)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.subplot(1, 2, 2)
    for df_y1s, muons, color in zip(dfs_y1s, muon_types, colors):
        plt.hist((df_y1s.query(y1s_truth))["mup_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^-)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.suptitle(f"{colls_y1s}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_muon_momentum_reso_{colls_y1s}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    i += 1; j += 1; k += 1; t += 1

#------------------------------------------------------------------------------------------------------------------
