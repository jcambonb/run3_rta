import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import mplhep
import sys 
import os

mplhep.styles.use(mplhep.styles.LHCb2)

# Data reading

pp_colls   = ["pp1.6", "pp7.6"]
PbPb_colls = ["PbPb_peripheral", "PbPb_peripheral"]

path = "../DaVinci_scripts/root_files"

files_jpsi_pp   = [f"Jpsi_mumu_{col}_exp24_tuple.root" for col in pp_colls]
files_jpsi_PbPb = [f"Jpsi_mumu_{col}_100_exp24_tuple.root" for col in PbPb_colls]
files_psi2s_pp  = [f"psi2s_mumu_{col}_exp24_tuple.root" for col in pp_colls]

muon_types = ["Long", "VUM", "VM"]

dtts  = [f"charmonium_mumu_{muon}" for muon in muon_types]

tdfs_jpsi_pp   = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
                  for colls, files in zip(pp_colls, files_jpsi_pp)}
tdfs_jpsi_PbPb = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
                  for colls, files in zip(PbPb_colls, files_jpsi_PbPb)}
tdfs_psi2s_pp  = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts] 
                  for colls, files in zip(pp_colls, files_psi2s_pp)}

tdfs_jpsi_pp = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_pp.items()}
tdfs_jpsi_pp = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_pp.items()}

tdfs_jpsi_PbPb = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_PbPb.items()}
tdfs_jpsi_PbPb = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_jpsi_PbPb.items()}

tdfs_psi2s_pp = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_psi2s_pp.items()}
tdfs_psi2s_pp = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] for colls, tdfs in tdfs_psi2s_pp.items()}

columns = ["mup_P", "mum_P", 
           "mup_MC_MOTHER_ID", "mum_MC_MOTHER_ID", 
           "mup_P_Res", "mum_P_Res"]

dfs_jpsi_pp   = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
                 for colls, tdfs in tdfs_jpsi_pp.items()}
dfs_jpsi_PbPb = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
                 for colls, tdfs in tdfs_jpsi_PbPb.items()}
dfs_psi2s_pp  = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["dimuon_M"])) for tdf in tdfs] 
                 for colls, tdfs in tdfs_psi2s_pp.items()}

jpsi_truth  = "mup_MC_MOTHER_ID == 443 & mum_MC_MOTHER_ID == 443"
psi2s_truth = "mup_MC_MOTHER_ID == 100443 & mum_MC_MOTHER_ID == 100443"

colors = ["black", "red", "blue"]

print("Total number of signal candidates")

for (colls1, dfs1), (colls2, dfs2), (colls3, dfs3) in zip(dfs_jpsi_pp.items(), dfs_psi2s_pp.items(), dfs_jpsi_PbPb.items()):
    
    print(f"JpsiToMuMu for {colls1}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs1):
        print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    ("#----------------------------------")
    
    print(" ")
    print(f"Psi2sToMuMu for {colls2}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs2):
        print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    print("#----------------------------------")

    print(" ")
    print(f"JpsiToMuMu for {colls3}")
    print("#----------------------------------")
    for muon, df in zip(muon_types, dfs3):
        print(f"Signal candidates for {muon} muons = {len(df["mup_P"])}")
    print("#----------------------------------")


# pp feature plots
#------------------------------------------------------------------------------------------------------------------
i = 1; j = 100; k = 1000; t = 10000

for (colls_jpsi, dfs_jpsi), (colls_psi2s, dfs_psi2s) in zip(dfs_jpsi_pp.items(), dfs_psi2s_pp.items()):
    plt.figure(i)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        plt.hist((df_jpsi.query(jpsi_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_mass_{colls_jpsi}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(j)
    for df_psi2s, muons, color in zip(dfs_psi2s, muon_types, colors):
        plt.hist((df_psi2s.query(psi2s_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_psi2s}  MagDown exp24")
    plt.savefig(f"./plots/psi2s_mass_{colls_psi2s}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(k)
    for df_jpsi, df_psi2s, muons, color in zip(dfs_jpsi, dfs_psi2s, muon_types, colors):
        plt.hist((df_jpsi.query(jpsi_truth))["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
        plt.hist((df_psi2s.query(psi2s_truth))["dimuon_M"], bins=100, histtype="step", color=color)
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/ccbar_dimuon_mass_{colls_jpsi}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(t)
    plt.subplot(1, 2, 1)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        plt.hist((df_jpsi.query(jpsi_truth))["mup_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^+)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.subplot(1, 2, 2)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        plt.hist((df_jpsi.query(jpsi_truth))["mum_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^-)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.suptitle(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_muon_momentum_reso_{colls_jpsi}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    i += 1; j += 1; k += 1; t += 1

#------------------------------------------------------------------------------------------------------------------

# PbPb feature plots
#------------------------------------------------------------------------------------------------------------------
for colls_jpsi, dfs_jpsi in dfs_jpsi_PbPb.items():
    plt.figure(i)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        plt.hist(df_jpsi["dimuon_M"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_mass_{colls_jpsi}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(t)
    plt.subplot(1, 2, 1)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        plt.hist((df_jpsi.query(jpsi_truth))["mup_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^+)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.subplot(1, 2, 2)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        plt.hist((df_jpsi.query(jpsi_truth))["mum_P_Res"], bins=100, histtype="step", color=color, label=f"{muons} muons")
    plt.xlabel(r"$\delta p/p(\mu^-)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.suptitle(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_muon_momentum_reso_{colls_jpsi}.pdf", dpi=300, bbox_inches='tight')
    plt.show()

    i += 1; j += 1; k += 1; t += 1
    
## Jpsi_mass vs Ecal energy
files_jpsi_PbPb = [f"Jpsi_mumu_{col}_exp24_tuple.root" for col in PbPb_colls]

dtts_old  = [f"Jpsimumu_{muon}" for muon in muon_types]

tdfs_jpsi_PbPb_old = {colls: [ROOT.RDataFrame(f"{dtt}/DecayTree", f"{path}/{files}") for dtt in dtts_old] 
                      for colls, files in zip(PbPb_colls, files_jpsi_PbPb)}

tdfs_jpsi_PbPb_old = {colls: [tdf.Define("mup_P_Res", "(mup_P-mup_TRUEP)/mup_TRUEP") for tdf in tdfs] 
                      for colls, tdfs in tdfs_jpsi_PbPb_old.items()}
tdfs_jpsi_PbPb_old = {colls: [tdf.Define("mum_P_Res", "(mum_P-mum_TRUEP)/mum_TRUEP") for tdf in tdfs] 
                      for colls, tdfs in tdfs_jpsi_PbPb_old.items()}

columns = ["mup_P", "mum_P", 
           "mup_MC_MOTHER_ID", "mum_MC_MOTHER_ID", 
           "mup_P_Res", "mum_P_Res"]

dfs_jpsi_PbPb_old = {colls: [pd.DataFrame(tdf.AsNumpy(columns=columns+["Jpsi_M", "eCalTot"])) for tdf in tdfs] 
                     for colls, tdfs in tdfs_jpsi_PbPb_old.items()}

cent_bins = { #"100-90":[ 0   , 310e3  ],  Usually keeping this annoying bin out
            "90_80": [310e3  , 800e3  ],  
            "80_70": [800e3  , 1750e3 ],  
            "70_60": [1750e3 , 3360e3 ],  
            "60_50": [3360e3 , 5900e3 ],  
            "50_40": [5900e3 , 9630e3 ],  
            "40_30": [9630e3 , 14860e3]} 
            #"30-20": [14860e3, 22150e3], Hopefully to be uncommented in few years
            #"20-10": [22150e3, 32280e3], 
            #"10-0 ": [32280e3 , 1e15]}
            
ecal_bins = list(set(bound for bins in cent_bins.values() for bound in bins))
ecal_bins = np.sort(ecal_bins)
ecal_intervals = [bins for bins in cent_bins.values()]

for colls_jpsi, dfs_jpsi in dfs_jpsi_PbPb_old.items():
    for df_jpsi, muons, color, n in zip(dfs_jpsi, muon_types, colors, [i+1e7 for i in range(len(muon_types))]):
        plt.figure(n)
        for range, bin in cent_bins.items():
            sel = f"{jpsi_truth} & eCalTot > {bin[0]} & eCalTot < {bin[1]}"
            plt.hist((df_jpsi.query(sel))["Jpsi_M"], bins=100, histtype="step", label=f"{range}")
        plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
        plt.ylabel("A.U")
        plt.legend()
        plt.title(f"{muons} muons {colls_jpsi}  MagDown exp24")
        plt.savefig(f"./plots/jpsi_mass_{colls_jpsi}_vs_eCalTot_{muon}.pdf", dpi=300, bbox_inches='tight')
        plt.show()         
    
for colls_jpsi, dfs_jpsi in dfs_jpsi_PbPb_old.items():
    plt.figure(i)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        ecaltot_hist, ecaltot_bins = np.histogram((df_jpsi.query(jpsi_truth))["eCalTot"], bins=ecal_bins)
        ecaltot_means = [(df_jpsi.query(f"{jpsi_truth} & eCalTot > {bin[0]} & eCalTot < {bin[1]}"))["eCalTot"].mean() 
                         for bin in cent_bins.values()]
        ecaltot_down = [mean-bin[0] for mean, bin in zip(ecaltot_means, cent_bins.values())]
        ecaltot_up   = [bin[1]-mean for mean, bin in zip(ecaltot_means, cent_bins.values())]

        plt.errorbar(ecaltot_means, ecaltot_hist, yerr=np.sqrt(ecaltot_hist), xerr=[ecaltot_down, ecaltot_up],
                     fmt=".", label=f"{muons} muons", capsize=3)
    
    plt.xlabel(r"ECAL energy [MeV]")
    plt.ylabel("Truth matched events")
    plt.title(f"{colls_jpsi}  MagDown exp24")
    plt.legend()
    plt.savefig(f"./plots/jpsi_{colls_jpsi}_events_vs_eCalTot.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(j)
    for df_jpsi, muons, color in zip(dfs_jpsi, muon_types, colors):
        plt.hist(df_jpsi["Jpsi_M"], bins=100, histtype="step", color=color, label=f"{muons} muons", density=True)
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"{colls_jpsi}  MagDown exp24")
    plt.savefig(f"./plots/jpsi_mass_{colls_jpsi}.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    i += 1; j += 1; k += 1; t += 1

#------------------------------------------------------------------------------------------------------------------

# PbPb and pp comparison
#------------------------------------------------------------------------------------------------------------------
for (colls_pp, dfs_pp), (colls_PbPb, dfs_PbPb) in zip(dfs_jpsi_pp.items(), dfs_jpsi_PbPb.items()):
    plt.figure(i)
    for df_pp, df_PbPb, muons, color in zip(dfs_pp, dfs_PbPb, muon_types, colors):
        if muons == "VUM":
            plt.hist((df_pp.query(jpsi_truth))["dimuon_M"], bins=100, histtype="step", color="black", label=f"{muons} {colls_pp}", density=True)
            plt.hist((df_PbPb.query(jpsi_truth))["dimuon_M"], bins=100, histtype="step", color="red", label=f"{muons} {colls_PbPb}", density=True)
    plt.xlabel(r"$m(\mu^+ \mu^-)$ [MeV/$c^2$]")
    plt.ylabel("A.U")
    plt.legend()
    plt.title(f"MagDown exp24")
    plt.savefig(f"./plots/jpsi_mass_{colls_pp}_{colls_PbPb}_comp.pdf", dpi=300, bbox_inches='tight')
    plt.show()
    
    plt.figure(t)
    plt.subplot(1, 2, 1)
    for df_pp, df_PbPb, muons, color in zip(dfs_pp, dfs_PbPb, muon_types, colors):
        if muons == "VUM":
            plt.hist((df_pp.query(jpsi_truth))["mup_P_Res"], bins=100, histtype="step", color="black", label=f"{muons} {colls_pp}", density=True)
            plt.hist((df_PbPb.query(jpsi_truth))["mup_P_Res"], bins=100, histtype="step", color="red", label=f"{muons} {colls_PbPb}", density=True)
    plt.xlabel(r"$\delta p/p(\mu^+)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.subplot(1, 2, 2)
    for df_pp, df_PbPb, muons, color in zip(dfs_pp, dfs_PbPb, muon_types, colors):
        if muons == "VUM":
            plt.hist((df_pp.query(jpsi_truth))["mum_P_Res"], bins=100, histtype="step", color="black", label=f"{muons} {colls_pp}", density=True)
            plt.hist((df_PbPb.query(jpsi_truth))["mum_P_Res"], bins=100, histtype="step", color="red", label=f"{muons} {colls_PbPb}", density=True)
    plt.xlabel(r"$\delta p/p(\mu^-)$")
    plt.ylabel("A.U")
    plt.legend()
    plt.suptitle(f"MagDown exp24")
    plt.savefig(f"./plots/jpsi_muon_momentum_reso_{colls_pp}_{colls_PbPb}_comp.pdf", dpi=300, bbox_inches='tight')
    plt.show()

    i += 1; j += 1; k += 1; t += 1
#------------------------------------------------------------------------------------------------------------------