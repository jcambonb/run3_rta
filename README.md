# Real Time Analysis (RTA) contributions for LHCb Run 3 data-taking period

Ivan Cambon Bouzas

Instituto Galego de Fisica de Altas Enerxias (IGFAE)

Universidade de Santiago de Compostela

LHCb Collaboration

## Introduction

This repository includes several RTA projects for LHCb Run 3 data-taking period as well as tutorials made by members of the collaboration for learning how to use the LHCb Run 3 software

## Requisites

Machines with `cvmfs` and `lbenv` are required as well as `Grid certificate` and `eos` for having access to the data files. For running the codes, `Moore` and `DaVinci` frameworks are used.

- `Moore` is expected to be run through the `lhcb stack`, in particular the branch `jcambonb-changes`. This branch also uses the `Rec` branch `jcambonb-changes`. Currently, a platform with `destdec` is mandatory. In order to use the `cvmfs` `Moore`, changes in the `Moore` option files are required.
- `DaVinci` is expected to be run through latest version in `cvmfs`. `stack` development version is not ready ye