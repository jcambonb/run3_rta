#-- GAUDI jobOptions generated on Thu Jul 11 11:54:18 2024
#-- Contains event types : 
#--   28142001 - 655 files - 1117659 events - 489.38 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim10c' 

#--  StepId : 179966 
#--  StepName : Digi17 for expected-2024+spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v45r0 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20200616.py;$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r426 
#--  Visible : N 

#--  Processing Pass: '/Sim10c' 

#--  StepId : 181118 
#--  StepName : Digi17 for expected-2024+spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v45r0 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20200616.py;$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r427 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000036_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000032_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000070_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000052_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000040_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000028_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000210_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000357_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000335_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000399_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000365_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000106_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000256_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000171_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000166_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000207_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000107_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000215_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000174_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000233_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000279_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000414_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217105/0000/00217105_00000113_1.digi',
], clear=True)
