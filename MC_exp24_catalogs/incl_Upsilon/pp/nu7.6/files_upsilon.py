#-- GAUDI jobOptions generated on Thu Jul 11 11:54:53 2024
#-- Contains event types : 
#--   18112001 - 600 files - 1027047 events - 433.34 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim10c' 

#--  StepId : 181118 
#--  StepName : Digi17 for expected-2024+spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v45r0 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20200616.py;$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r427 
#--  Visible : N 

#--  Processing Pass: '/Sim10c' 

#--  StepId : 179966 
#--  StepName : Digi17 for expected-2024+spillover 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v45r0 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20200616.py;$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py;$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r426 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000005_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000047_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000042_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000274_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000041_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000272_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000098_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000160_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000229_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000030_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000236_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000059_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000180_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000167_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000013_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000021_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000110_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000102_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000188_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000104_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000046_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000017_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000183_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000128_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000111_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000120_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000077_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000018_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000181_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000147_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000045_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00217103/0000/00217103_00000170_1.digi',
], clear=True)
