from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000026_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000028_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000068_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000032_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000036_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000079_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000048_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000087_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000065_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000025_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000046_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000040_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000030_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000010_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000083_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000038_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000039_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000063_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000014_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000023_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000086_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000019_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000047_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000059_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000004_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000084_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000080_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000070_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000076_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000075_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000044_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000031_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000029_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000082_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000049_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000057_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000072_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000078_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000035_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000034_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000073_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000058_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000045_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000081_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000021_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000022_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000085_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000052_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000043_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000001_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000009_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000037_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000054_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000041_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000033_1.digi',
'LFN:/lhcb/MC/Dev/DIGI/00205655/0000/00205655_00000060_1.digi',
], clear=True)
