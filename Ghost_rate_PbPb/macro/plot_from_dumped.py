import os, sys
import argparse
import ROOT
from ROOT import TMultiGraph, TMath, TAxis, TH1, TH1F, TH2F, TLatex, TGaxis, TROOT, TSystem, TCanvas, TFile, TTree, TObject, TGraph, TLegend
from ROOT import kDashed, kRed, kGreen, kBlue, kBlack, kAzure, kTRUE, kFALSE, gPad, SetOwnership
from ROOT import gROOT, gStyle, TStyle, TPaveText
from ROOT import ROOT, RDataFrame, vector, TGraphAsymmErrors
import logging

gROOT.SetBatch(True)


# load LHCb style
sys.path.append(os.path.abspath(os.getcwd()))
from LHCbStyle import setLHCbStyle
from Legend import place_legend
setLHCbStyle()


# constants and definions


# variables for the plot
vars = (
	# "nPrimaryVertex",
	'p',
    'pt',
    'ghostProb',
    'chi2ndof', # to be implemented (needed to divide 'chi2' and 'ndof')
	"eta",
    'GEC',
    #"VeloHits" ,
    #"UTHits" ,
    #"FTHits",
	# "P:Eta",
	# "pt:Eta",
	# "P:FTHits",
	# "P:VeloHits",
	# "P:UTHits"
)

varTitle = {
	# "nPrimaryVertex",
	'p':'#font[12]{p} [MeV/#font[12]{c}]',
    'pt':'#font[12]{p}_T [MeV/#font[12]{c}]',
    'ghostProb':'Ghost Prob.',
    'chi2ndof':'#chi^{2}/nDoF', # to be implemented (needed to divide 'chi2' and 'ndof')
	"eta":'#eta',
    'GEC':'GEC',
    #"VeloHits" ,
    #"UTHits" ,
    #"FTHits",
	# "P:Eta",
	# "pt:Eta",
	# "P:FTHits",
	# "P:VeloHits",
	# "P:UTHits"
}

def getTrackers():
    return [
        "BestLong",
        #"Forward",
        ## "ForwardHLT1",
        ## "Downstream",
        #"Match",
        #"Seed",
        ## "Upstream",
        #"Velo",        
        ## "VeloFull",
        ]

def getTrackTag():
    return [
        "Velo",
        "Upstream",
        "Forward",
        "Seed",
        ]

def getCondition(tag):
    conditions = {
        "Velo": "hasVelo",
        "Upstream": "hasVelo & hasTT",
        "Forward": "hasVeloAndT",
        "Seed": "hasT",
    }

    return conditions[tag]


rang = { "FT_x":[-1450., -110],
         "p":[1, 100000],
         "pt":[1, 5000],
         "eta":[1., 7.],
         "GEC":[0, 60000],
         #'ghostProb':[0.95,0.99,],#[0.95,1,],
         'ghostProb':[0.99998,1],
         #'ghostProb':[0.9999,1,],
         'chi2ndof':[0,3.2],
}

# main macro
if __name__ == '__main__':

    tupleFile = '/home/llr/phenix/boente/run3/rta_pbpb/macro/tuple_dumped/BestLongRec_hlt2_lead_lead_evt200_pr_kf_gec60000_light_reco_monitor_fast_reco_no_ut.root'  #Dumper_recTracks_ManyEvent.root'
    #tupleFile = '/home/llr/phenix/boente/run3/rta_pbpb/macro/tuple_dumped/BestLongRec_hlt2_lead_lead_evt200_pr_kf_gec60000_light_reco_monitor_fast_reco_no_seeding_no_ut.root'
    #/home/llr/phenix/boente/run3/rta_pbpb/macro/tuple_dumped/Dumper_recTracks_hlt2_lead_lead_pr_kf_gec60000_light_reco_monitor_fast_reco_no_ut_5evts.root

    f = TFile( tupleFile, 'read')
    t = f.Get( 'Hits_detectors' )
    #t = ROOT.RDataFrame( "Hits_detectors", tupleFile )

    tag = "BestLong" # select type of track that is in the tuple file
    tupleName = tupleFile.split('/tuple_dumped/')[-1].replace(".root", "")

    GEC = 60000
    #var = 'ghostProb' #"FT_x"
    #selection = "FT_z>8500 && FT_z<8730 "
    #selection = "1==1&&evt_nFTHits>20000"
    selections = [ "1==1", "1==1&&evt_nFTHits>30000", "1==1&&evt_nFTHits>20000&&evt_nFTHits<30000", "1==1&&evt_nFTHits<20000",  ]

    for selection in selections:
        #tsel  = t.Filter(selection)
        #tselsignal = tsel.Filter("isMatched==1")
        #tselghost = tsel.Filter("isMatched==0")

        for var in vars:
            if selection!="1==1": selName = selection.split("1==1&&")[1].replace( "&&", "_").replace("<", "lower").replace(">", "greater")
            else: selName = ""
            
            h_signal = TH1F( "h_signal"+var+selName, "h_signal"+var+selName, 150, rang[var][0], rang[var][1] )
            h_ghost = TH1F( "h_ghost"+var+selName, "h_ghost"+var+selName, 150, rang[var][0], rang[var][1] )
            h_total = TH1F( "h_total"+var+selName, "h_total"+var+selName, 150, rang[var][0], rang[var][1] )

            h_signal.Sumw2()
            h_ghost.Sumw2()
            h_total.Sumw2()
            '''
            ev = t
            print("total tracks: "+str(t.GetEntries()))
            for e in range(0, t.GetEntries()):
                ev.GetEntry(e)
                if not e%1000: print("Entry "+str(e))

                nHits = len(ev.FT_x)
                for i in range(0, nHits):
                    if not (ev.FT_z[i]  > 8500 and  ev.FT_z[i] < 8730): continue

                    h_total.Fill( ev.FT_x[i] )
                    if not ev.isMatched: h_ghost.Fill( ev.FT_x[i] )
            '''
            if var=='chi2ndof': _var = "chi2/ndof"
            elif var=='GEC': _var = "evt_nFTHits+evt_nUTHits"
            else: _var = var
       
            t.Project( "h_signal"+var+selName, _var, "isMatched==1 &&" + selection)
            t.Project( "h_ghost"+var+selName, _var, "isMatched==0 &&" + selection)
            t.Project( "h_total"+var+selName, _var, selection)
            
            '''
            h_total = tsel.Histo1D(ROOT.RDF.TH1DModel( "h_total"+var+selName, "h_total"+var+selName, 150, rang[var][0], rang[var][1] ), var ).GetValue()
            h_signal = tselsignal.Histo1D(ROOT.RDF.TH1DModel( "h_signal"+var+selName, "h_signal"+var+selName, 150, rang[var][0], rang[var][1] ), var ).GetValue()
            h_ghost = tselghost.Histo1D(ROOT.RDF.TH1DModel( "h_ghost"+var+selName, "h_ghost"+var+selName, 150, rang[var][0], rang[var][1] ), var ).GetValue()
            ''' 

            h_signal.Sumw2()
            h_ghost.Sumw2()
            h_total.Sumw2()

            # print( h_ghost.GetEntries() )

            # Ghost rate plot
            g_ghost = TGraphAsymmErrors()
            g_ghost.SetName("GhostRate"+selName)
            g_ghost.Divide(h_ghost, h_total,"cl=0.683 b(1,1) mode")

            name = "ghost rate vs. " + var 
            title = "ghost rate vs. " + var
            canvas = TCanvas(name, title)


            mg = g_ghost
            SetOwnership(mg,False)
            mg.Draw()
            mg.GetXaxis().SetTitle(var)
            mg.GetYaxis().SetTitle("Ghost Rate"+selName)
            mg.GetYaxis().SetRangeUser(0, 1.05)

            mg.SetName("Ghost rate vs. " + var)
            #mg.SetTitle()

            pt = TPaveText(0.25, 0.75, 0.5, 0.8, "BRNDC")
            pt.SetTextSize(0.05)
            pt.SetBorderSize(1)
            pt.SetFillColor(10)
            pt.SetLineWidth(0)
            text = pt.AddText(tag+" tracks, GEC = "+str(GEC))

            if selection!="1==1":
                selToshow = selection.split("1==1&&")[1]
                
                selText = TPaveText(0.25, 0.80, 0.5, 0.85, "BRNDC")
                selText.SetTextSize(0.05)
                selText.SetBorderSize(1)
                selText.SetFillColor(10)
                selText.SetLineWidth(0)
                text2 = selText.AddText( selToshow )
                selText.Draw()

            pt.Draw()
            canvas.SetRightMargin(0.05)
            #place_legend( canvas )
            if not os.path.isdir( "outputs/"+tupleName ): os.system("mkdir "+"outputs/"+tupleName)
            canvas.SaveAs( "outputs/"+tupleName+"/"+"ghostrate_vs_"+var+'_'+selName+'.pdf' )

            # True and ghost yields plot

            name = "True and ghost vs. " + var 
            title = "True and ghost vs. " + var
            canvas = TCanvas(name, title)

            mg = h_ghost
            SetOwnership(mg, False)
            mg.GetXaxis().SetTitle(varTitle[var])
            mg.GetYaxis().SetTitle("Tracks")
            mg.SetLineColor(kBlue)
            mg.SetMarkerColor(kBlue)
            mg.Draw('p')

            if var=='ghostProb':
                mg.SetMinimum(0.1)
            gPad.SetLogy()

            h_signal.SetLineColor(kRed)
            h_signal.SetMarkerColor(kRed)
            h_signal.Draw('same p')

            h_signal.SetName( 'True tracks')
            h_signal.SetTitle( 'True tracks')
            mg.SetName( 'Ghost tracks')
            mg.SetTitle( 'Ghost tracks')

            mg.SetName("True and ghost vs. " + var)
            #mg.SetTitle()

            pt = TPaveText(0.25, 0.80, 0.5, 0.85, "BRNDC")
            pt.SetTextSize(0.05)
            pt.SetBorderSize(1)
            pt.SetFillColor(10)
            pt.SetLineWidth(0)
            text = pt.AddText(tag+" tracks, GEC = "+str(GEC))
            pt.Draw()

            if selection!="1==1":
                selToshow = selection.split("1==1&&")[1].replace('&&', '; ')
                
                selText = TPaveText(0.3, 0.75, 0.55, 0.8, "BRNDC")
                selText.SetTextSize(0.05)
                selText.SetBorderSize(1)
                selText.SetFillColor(10)
                selText.SetLineWidth(0)
                text2 = selText.AddText( selToshow )
                selText.Draw()
            
            canvas.SetRightMargin(0.05)
            place_legend( canvas,
                    x1=0.25,
                    y1=0.6,
                    x2=0.45,
                    y2=0.75,)
            if not os.path.isdir( "outputs/"+tupleName ): os.system("mkdir "+"outputs/"+tupleName)
            canvas.SaveAs( "outputs/"+tupleName+"/"+"trueandghost_vs_"+var+'_'+selName+'.pdf' ) #_noseeding

    #canvas.Write()