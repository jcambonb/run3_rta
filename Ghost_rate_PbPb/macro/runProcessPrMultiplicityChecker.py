import os


mc_decay  = "minbias"
monitor_path = "../HLT2_Reco/"

gecs      = [30000]
nevts     = [200] 
ref_nevts = [200]

options     = [f"light_reco_monitor_fast_reco_sim24{mc_decay}"]
ref_options = [f"light_reco_monitor_fast_reco_no_ut_sim24{mc_decay}"]

legs     = ["with UT, ghostP<1.0", "with UT, ghostP<0.5"]
ref_legs = ["without UT,ghostP<1.0", "without UT, ghostP<0.5", "with UT, ghostP<0.5"]

sels     = ["ghostprob1.0", "ghostprob0.5"] 
ref_sels = ["ghostprob1.0", "ghostprob0.5", "ghostprob1.0"]

out     = ["ut_max_ghostp_1", "ut_max_ghostp_0.5"]  
ref_out = ["no_ut_max_ghostp_1", "no_ut_max_ghostp_0.5", "ut_max_ghostp_0.5"]  

directory    = f"{monitor_path}/monitor_files/" 
refdirectory = directory

for gec in gecs:
    i = 0
    for opt in options:
        for sel in sels:
            for evts in nevts:
                if  sel == None: fixsel = ""
                else: fixsel = sel.replace(" ","").replace("(","").replace(")","").replace("&","_").replace(">","greater").replace("<","lower")

                file = f"hlt2_PbPb_evt{evts}_pr_kf_gec{gec}_{opt}_{fixsel}"
                
                if ref_options:
                    j = 0
                    for ref_opt in ref_options:
                        for ref_sel in ref_sels:
                            for ref_evts in ref_nevts:
                                if ref_sel == "same": fixsel2 = fixsel
                                elif ref_sel == None: fixsel2 = ""
                                else: fixsel2 = ref_sel.replace(" ","").replace("(","").replace(")","").replace("&","_").replace(">","greater").replace("<","lower")

                                file1 = f"hlt2_PbPb_evt{evts}_pr_kf_gec{gec}_{opt}_{fixsel}"
                                file2 = f"hlt2_PbPb_evt{ref_evts}_pr_kf_gec{gec}_{ref_opt}_{fixsel2}"

                                leg1 = legs[i]
                                leg2 = ref_legs[j]
                                
                                output_file = f"{out[i]}_comp_{ref_out[j]}"
                                print(output_file)
                                print("------------------------------------------------------------------------------------------------------------")
                                print( "Running PrMultiplicityChecker with GEC"+str(gec)+"  options: "+opt+ "  Selection: "+fixsel)
                                os.system(f"../../../stack/Moore/run python ProcessPrMutliplicityChecker.py --directory='"+directory+"' --directory2='"+refdirectory+"' --file='"+file1+"' --leg='"+leg1+"' --gec='"+str(gec)+"' --options='"+str(opt)+"' --compare=True --file2='"+file2+"' --leg2='"+leg2+"' --outputfile='"+output_file+"'   " ) 
                                print("------------------------------------------------------------------------------------------------------------")      
                                  
                            j += 1
                    
                else: 
                    print( "Running PrMultiplicityChecker with GEC"+str(gec)+"  options: "+opt+ "  Selection: "+fixsel)                
                    os.system( "../../../stack/Moore/run python ProcessPrMutliplicityChecker.py --directory='"+directory+"' --file='"+file+"' --leg='"+legs[i]+"' --gec='"+str(gec)+"' --options='"+str(opt)+"' --compare=False" ) 
            
            i += 1