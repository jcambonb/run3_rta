###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/python

# Script for processing the output of PrMultiplicityChecker
# and compute the reconstructed efficiency
#
# The efficency is calculated usig TGraphAsymmErrors in case of 1D distribution
# otherwise drawn as TH2.
# and Bayesian error bars
#
# author: Benjamin Audurier (benjamin.audurier@cern.ch)
# date:   07/2021
#

import os, sys
import argparse
import ROOT
from ROOT import TMultiGraph, TMath, TAxis, TH1, TH1F, TH2F, TLatex, TGaxis, TROOT, TSystem, TCanvas, TFile, TTree, TObject, TGraph, TLegend
from ROOT import kDashed, kRed, kGreen, kBlue, kBlack, kAzure, kTRUE, kFALSE, gPad, SetOwnership
from ROOT import gROOT, gStyle, TStyle, TPaveText
from ROOT import ROOT, RDataFrame, vector, TGraphAsymmErrors, TGraphErrors
import logging

gROOT.SetBatch(True)

# load LHCb style
sys.path.append(os.path.abspath(os.getcwd()))
from LHCbStyle import setLHCbStyle
from Legend import place_legend
setLHCbStyle()

# constants and definions

# variables for the plot
vars = (
	# "nPrimaryVertex", #add ghost prob, add 2D variables to set cut in P and Pt
	'P',
    'Pt',
	"Eta",
    "VeloHits" ,
    "UTHits" ,
    "FTHits",
    "FTHitsP_5GeV",
	# "P:Eta",
	# "pt:Eta",
	# "P:FTHits",
	# "P:VeloHits",
	# "P:UTHits"
)
rebinningVars = [ "VeloHits" , "UTHits" , "FTHits", "FTHitsP" ]
rebinning = 2 # number of bins to merge

def getTrackers():
    return [
        "BestLong",
        "BestDownstream",
        "Forward",
        #"ForwardHLT1",
        ## "Downstream",
        #"Match",
        #"Seed",
        "Upstream",
        "Velo",        
        ## "VeloFull",
        ]

def getTrackTag():
    return [
        "Velo",
        "Upstream",
        "Forward",
        "Seed",
        ]

def getCondition(tag):
    conditions = {
        "Velo": "hasVelo",
        "Upstream": "hasVelo & hasTT",
        "Forward": "hasVeloAndT",
        "Seed": "hasT",
    }

    return conditions[tag]


def getOriginFolders():
    basedict = {
        "Velo": {},
        "Upstream": {},
        "Forward": {},
        "Downstream": {},
        "DownstreamUTHits": {},
        "ForwardUTHits": {},
        "ForwardHlt1": {},
        "Match": {},
        "MatchUTHits": {},
        "Seed": {},
        "VeloFull": {},
        "Best": {},
        "BestLong": {},
        "LongGhostFiltered": {},
        "BestDownstream": {},
        "DownstreamGhostFiltered": {},
        "CaloElectrons": {}
    }

    basedict["Downstream"]['folder']              = "DownstreamMultiplicityChecker"
    basedict["DownstreamUTHits"]['folder']        = "DownstreamUTHitsChecker"
    basedict["Forward"]['folder']                 = "ForwardMultiplicityChecker"
    basedict["ForwardUTHits"]['folder']           = "ForwardUTHitsChecker"
    basedict["ForwardHlt1"]['folder']             = "ForwardHlt1MultiplicityChecker"
    basedict["Match"]['folder']                   = "MatchMultiplicityChecker"
    basedict["MatchUTHits"]['folder']             = "MatchUTHitsChecker"
    basedict["Seed"]['folder']                    = "SeedMultiplicityChecker"
    basedict["Upstream"]['folder']                = "UpstreamMultiplicityChecker"
    basedict["Velo"]['folder']                    = "VeloMultiplicityChecker"
    basedict["VeloFull"]['folder']                = "VeloFullMultiplicityChecker"
    basedict["Best"]['folder']                    = "BestMultiplicityChecker"
    basedict["BestLong"]['folder']                = "BestLongMultiplicityChecker"
    basedict["LongGhostFiltered"]['folder']       = "LongGhostFilteredMultiplicityChecker"
    basedict["BestDownstream"]['folder']          = "BestDownstreamMultiplicityChecker"
    basedict["DownstreamGhostFiltered"]['folder'] = "DownstreamGhostFilteredMultiplicityChecker"
    basedict["CaloElectrons"]['folder']           = "CaloElectronsMultiplicityChecker"

    return basedict

# define the parser
def argument_parser():
    parser = argparse.ArgumentParser(description="location of the tuple file")
    parser.add_argument(
        '--directory',
        type=str,
        default=os.getcwd(),
        help='location of input tuple files')
    parser.add_argument(
        '--file',
        type=str,
        default="hlt2_lead_lead_track_monitoring_with_mc",
        help='name input tuple files')
    parser.add_argument(
        '--gec',
        type=str,
        default="None",
        help='Global Event Cut applied to the reconstruction')
    parser.add_argument(
        '--leg',
        type=str,
        default="",
        help='Tag of the reconstruction type for legend (track selection, particularity of reconstruction)')
    parser.add_argument(
        '--options',
        type=str,
        default="None",
        help='Options used to run the reconstruction')
    parser.add_argument(
        '--compare',
        type=str,
        default="False",
        help='Set to True if two reconstruction outputs are to be compared')
    parser.add_argument(
        '--directory2',
        type=str,
        default=os.getcwd(),
        help='location of input tuple files to be compared with')
    parser.add_argument(
        '--file2',
        type=str,
        default="hlt2_lead_lead_track_monitoring_with_mc",
        help='name input tuple files to be compared with')
    parser.add_argument(
        '--leg2',
        type=str,
        default="",
        help='Tag of the reconstruction type 2 for legend (track selection, particularity of reconstruction)')
    parser.add_argument(
        '--outputfile',
        type=str,
        default="hlt2_lead_lead_track_monitoring_with_mc",
        help="name output files")
    return parser


def compute_1D_efficiency( _var, _repo, refTag="" ):
    print("-- %s - %s - %s" % (tracker, _var, tag))

    # configure canvas
    name = "efficiency vs. " + _var + " - matching " + tag
    title = "efficiency vs. " + _var + ", " + tracker + ", " + tag

    _pers_var = _var

    # get variable for 2D case  
    if _var in [ "FTHitsP_5GeV" ]:
        _var, cut = _var.split("_")[0], _var.split("_")[1]
    
    # get histos
    histoName = tag + '_' + _var
    numeratorName = histoName + "_reconstructed"
    numerator = _repo.Get(numeratorName)
    denominatorName = histoName + "_reconstructible"
    denominator = _repo.Get(denominatorName)

    if _pers_var in rebinningVars:
        numerator.Rebin( rebinning )
        denominator.Rebin( rebinning )
    
    try:
        assert numerator.GetEntries() > 0 and denominator.GetEntries() > 0
    except AttributeError:
        print("cannot find histos, continue ...")
        return
    #numerator.Sumw2()
    #denominator.Sumw2()

    if _var in [ "FTHitsP" ]:
        # for this case, numerator and denominator are 2D histograms
        factor = 1
        if "GeV" in cut: factor = 1000
        cutVal = float( cut.split("GeV")[0] )*factor

        numerator = numerator.ProjectionX ( numeratorName+"_px"+refTag, numerator.GetYaxis().FindBin( cutVal ), -1, "e" )
        denominator = denominator.ProjectionX ( denominatorName+"_px"+refTag, denominator.GetYaxis().FindBin( cutVal ), -1, "e" )
        if _var in rebinningVars:
            numerator.Rebin( rebinning )
            denominator.Rebin( rebinning )
            
    # Efficiency plot
    g_efficiency = TGraphAsymmErrors()
    #g_efficiency = TGraphErrors()
    g_efficiency.SetName("efficiency"+refTag)
    g_efficiency.Divide(numerator, denominator,"cl=0.683 b(1,1) mode")
    return g_efficiency, denominator


def plot_1D_efficiency( _var, g_efficiency, denominator, g_efficiency_2=None, denominator2=None ):
    name = "efficiency vs. " + _var + " - matching " + tag
    title = "efficiency vs. " + _var + ", " + tracker + ", " + tag
    canvas = TCanvas(name, title)

    # get variable for 2D case  
    if _var in [ "FTHitsP_5GeV" ]: 
        _varTitle = "FTHits"
    else: _varTitle = _var

    mg = g_efficiency
    #mg = TMultiGraph()
    #SetOwnership(mg,False)
    #mg.Add(g_efficiency)
    mg.Draw("AP")
    mg.GetXaxis().SetTitle(_varTitle)
    mg.GetYaxis().SetTitle("Efficiency")
    mg.GetYaxis().SetRangeUser(0, 1.05)
    if "BestDownstream" in tracker: mg.GetYaxis().SetRangeUser(0, 0.1)

    mg.SetName("efficiency vs. " + _var)
    mg.SetTitle(args.leg)
    mg.SetMarkerColor(kBlack)

    if args.compare=="True":
        mg2 = g_efficiency_2
        mg2.Draw("P same")

        mg2.SetName("efficiency vs. " + _var)
        mg2.SetTitle(args.leg2)
        mg2.SetMarkerStyle(24)
        mg2.SetLineColor(kBlue)
        mg2.SetLineColorAlpha(kBlue, 0.6)
        mg2.SetMarkerColor(kBlue)
        mg2.SetMarkerColorAlpha(kBlue, 0.6)


    if args.compare=="True":
        rightmax1 = 1.05 * denominator.GetMaximum()
        rightmax2 = 1.05 * denominator2.GetMaximum()
        rightmax = max( [rightmax1, rightmax2 ] )
        scale = gPad.GetUymax() / rightmax

        denominator = denominator.Clone()
        SetOwnership(denominator,False)
        denominator.Scale(scale)
        denominator.SetName("inputs "+args.leg)
        denominator.SetTitle("inputs "+args.leg)
        denominator.SetFillStyle(3004)
        denominator.SetFillColorAlpha(kRed-10, 0.6)
        denominator.SetLineColorAlpha(kRed-10,0.6)
        denominator.SetLineWidth(2)

        denominator2 = denominator2.Clone()
        SetOwnership(denominator2,False)
        denominator2.Scale(scale)
        denominator2.SetName("inputs "+args.leg2)
        denominator2.SetTitle("inputs "+args.leg2)
        denominator2.SetFillStyle(3003)
        denominator2.SetFillColorAlpha(kBlue-10, 0.6)
        denominator2.SetLineColorAlpha(kBlue-10,0.6)
        denominator2.SetLineWidth(2)

        denominator.Draw("hist same")
        denominator2.Draw("hist same")
    else:
        rightmax = 1.05 * denominator.GetMaximum()
        scale = gPad.GetUymax() / rightmax
        denominator = denominator.Clone()
        SetOwnership(denominator,False)
        denominator.Scale(scale)
        denominator.SetName("inputs")
        denominator.SetTitle("inputs")
        denominator.SetFillStyle(3004)
        denominator.SetFillColorAlpha(kBlue-10, 0.6)
        denominator.SetLineColorAlpha(kBlue-10,0.6)
        denominator.SetLineWidth(2)
        denominator.Draw("hist same")
    
    #mg.Draw("P same")
    #mg2.Draw("P same")

    axis = TGaxis(gPad.GetUxmax(), 0, gPad.GetUxmax(), 1.05, 0,
                  rightmax, 510, "+L")
    if _var == "P":
        axis = TGaxis(50000, 0, 50000, 1.05, 0, rightmax, 510,"+L")
        mg.GetXaxis().SetRangeUser(0, 50000)
    if _var == "Pt":
        axis = TGaxis(5000, 0, 5000, 1.05, 0, rightmax, 510, "+L")
        mg.GetXaxis().SetRangeUser(0, 5000)
    axis.SetTitle("Number of events")
    axis.SetTextFont(132)
    axis.SetLabelFont(132)
    axis.SetTitleFont(132)
    axis.SetLabelSize(0.06)
    axis.SetTitleSize(0.07)
    axis.SetNdivisions(505)
    gPad.SetRightMargin(0.75)

    # Legend
    pt = TPaveText(0.45, 0.82, 0.83, 0.97, "BRNDC")
    pt.SetTextSize(0.05)
    pt.SetBorderSize(1)
    pt.SetFillColor(10)
    pt.SetLineWidth(0)
    text = pt.AddText( tag+" tracks from "+tracker+", GEC = "+str(GEC))

    pt2 = TPaveText(0.55, 0.8, 0.78, 0.86, "BRNDC")
    pt2.SetTextSize(0.05)
    pt2.SetBorderSize(1)
    pt2.SetFillColor(10)
    pt2.SetLineWidth(0)
    text2 = pt2.AddText( "LHCb simulation, PbPb")

    ## Write cut in 2D histogram
    if _var in [ "FTHitsP_5GeV" ]: 
        cut = _var.split("_")[1]
        varCut = _var.split("_")[0].split(_varTitle)[1]
        
        cutpt = TPaveText(0.55, 0.79, 0.83, 0.84, "BRNDC")
        cutpt.SetTextSize(0.04)
        cutpt.SetBorderSize(1)
        cutpt.SetFillColor(10)
        cutpt.SetLineWidth(0)
        cuttext = cutpt.AddText( varCut+">"+cut )
    
    topt = TPaveText(0.45, 0.75, 0.95, 0.8, "BRNDC")
    topt.SetTextSize(0.035)
    topt.SetBorderSize(1)
    topt.SetFillColor(10)
    topt.SetLineWidth(0)
    text = topt.AddText("Options: "+opts)

    place_legend( canvas, x1=0.55, y1=0.65, x2=0.89, y2=0.79  )
    #pt.AddEntry( Signal, "True tracks", "l")
    #pt.AddEntry( ghost, "Ghost tracks", "l")

    pt.Draw()
    pt2.Draw()
    if _var in [ "FTHitsP_5GeV" ]: cutpt.Draw()
    #topt.Draw()

    canvas.SetRightMargin(0.05)
    #place_legend( canvas)
    canvas.SaveAs( './outputs/eff_plots_mymacro_'+args.outputfile+'/'+"efficiency_vs_"+_var+"_"+tracker+"_"+tag+'.pdf' )
    canvas.Write()

def compute_1D_ghost_rate( _var, _repo ):
    # configure canvas
    name = "ghost rate vs. " + _var 
    title = "ghost rate vs. " + _var + ", " + tracker 

    _pers_var = _var

    # get variable for 2D case  
    if _var in [ "FTHitsP_5GeV" ]: 
        _var, cut = _var.split("_")[0], _var.split("_")[1]

    # get histos    
    histoName = _var
    numeratorName = histoName + "_Ghosts"
    numerator = _repo.Get(numeratorName)
    denominatorName = histoName + "_Total"
    denominator = _repo.Get(denominatorName)

    if _pers_var in rebinningVars:
        numerator.Rebin( 3 )
        denominator.Rebin( 3 )

    try:
        assert numerator.GetEntries() > 0 and denominator.GetEntries() > 0
    except AttributeError:
        print("cannot find histos, continue ...")
        return

    if (_var in [ "FTHitsP" ]):
        # for this case, numerator and denominator are 2D histograms
        factor = 1
        if "GeV" in cut: factor = 1000
        cutVal = float( cut.split("GeV")[0] )*factor

        numerator = numerator.ProjectionX ( numeratorName+"_px", numerator.GetYaxis().FindBin( cutVal ), -1, "e" )
        denominator = denominator.ProjectionX ( denominatorName+"_px", denominator.GetYaxis().FindBin( cutVal ), -1, "e" )
        
        if _var in rebinningVars:
            numerator.Rebin( 3 )
            denominator.Rebin( 3 )


    # Efficiency plot
    g_ghost = TGraphAsymmErrors()
    #g_ghost = TGraphErrors()
    g_ghost.SetName("GhostRate")
    g_ghost.Divide( numerator, denominator, "cl=0.683 b(1,1) mode")
    return g_ghost, denominator

def plot_1D_ghost_rate( _var, g_ghost, denominator, g_ghost_2=None, denominator2=None):
    name = "ghost rate vs. " + _var 
    title = "ghost rate vs. " + _var + ", " + tracker 
    canvas = TCanvas(name, title)

    
    mg = g_ghost
    #mg = TMultiGraph()
    #SetOwnership(mg,False)
    #mg.Add(g_ghost)

    # get variable for 2D case  
    if _var in [ "FTHitsP_5GeV" ]: 
        _varTitle = "FTHits"
    else: _varTitle = _var

    mg.Draw("AP")
    mg.GetXaxis().SetTitle(_varTitle)
    mg.GetYaxis().SetTitle("Ghost Rate")
    mg.GetYaxis().SetRangeUser(0, 1.05)
    

    mg.SetName("Ghost rate vs. " + _var)
    mg.SetTitle(args.leg)
    mg.SetMarkerColor(kBlack)

    if args.compare=="True":
        mg2 = g_ghost_2
        mg2.Draw("P same")

        mg2.SetName("efficiency vs. " + _var)
        mg2.SetTitle(args.leg2)
        mg2.SetMarkerStyle(24)
        mg2.SetLineColor(kBlue)
        mg2.SetLineColorAlpha(kBlue, 0.6)
        mg2.SetMarkerColor(kBlue)


    if args.compare=="True":
        rightmax1 = 1.05 * denominator.GetMaximum()
        rightmax2 = 1.05 * denominator2.GetMaximum()
        rightmax = max( [rightmax1, rightmax2 ] )
        scale = gPad.GetUymax() / rightmax

        denominator = denominator.Clone()
        SetOwnership(denominator,False)
        denominator.Scale(scale)
        denominator.SetName("inputs "+args.leg)
        denominator.SetTitle("inputs "+args.leg)
        denominator.SetFillStyle(3004)
        denominator.SetFillColorAlpha(kRed-10, 0.6)
        denominator.SetLineColorAlpha(kRed-10,0.6)
        denominator.SetLineWidth(2)

        denominator2 = denominator2.Clone()
        SetOwnership(denominator2,False)
        denominator2.Scale(scale)
        denominator2.SetName("inputs "+args.leg2)
        denominator2.SetTitle("inputs "+args.leg2)
        denominator2.SetFillStyle(3003)
        denominator2.SetFillColorAlpha(kBlue-10, 0.6)
        denominator2.SetLineColorAlpha(kBlue-10,0.6)
        denominator2.SetLineWidth(2)

        denominator.Draw("hist same")
        denominator2.Draw("hist same")
    else:
        rightmax = 1.05 * denominator.GetMaximum()
        scale = gPad.GetUymax() / rightmax
        denominator = denominator.Clone()
        SetOwnership(denominator,False)
        denominator.Scale(scale)
        denominator.SetName("inputs")
        denominator.SetTitle("inputs")
        denominator.SetFillStyle(3004)
        denominator.SetFillColor(kBlue)
        denominator.SetFillColorAlpha(kBlue, 0.6)
        denominator.SetLineColor(kBlue - 10)
        denominator.SetLineWidth(2)
        denominator.Draw("hist same")


    #mg.Draw("AP same")
    #if args.compare=="True": mg2.Draw("P same")

    axis = TGaxis(gPad.GetUxmax(), 0, gPad.GetUxmax(), 1.05, 0,
                  rightmax, 510, "+L")
    if _var == "P":
        axis = TGaxis(50000, 0, 50000, 1.05, 0, rightmax, 510,"+L")
        mg.GetXaxis().SetRangeUser(0, 50000)
    if _var == "Pt":
        axis = TGaxis(5000, 0, 5000, 1.05, 0, rightmax, 510, "+L")
        mg.GetXaxis().SetRangeUser(0, 5000)
    axis.SetTitle("Number of events")
    axis.SetTextFont(132)
    axis.SetLabelFont(132)
    axis.SetTitleFont(132)
    axis.SetLabelSize(0.06)
    axis.SetTitleSize(0.07)
    axis.SetNdivisions(505)
    gPad.SetRightMargin(0.75)


    # Legend
    #pt = TPaveText(0.45, 0.4, 0.88, 0.47, "BRNDC")#0.25, 0.80, 0.45, 0.95, "BRNDC")
    pt = TPaveText(0.15, 0.88, 0.58, 0.95, "BRNDC")#0.25, 0.80, 0.45, 0.95, "BRNDC")
    pt.SetTextSize(0.05)
    pt.SetBorderSize(1)
    pt.SetFillColor(10)
    pt.SetLineWidth(0)
    text = pt.AddText(tracker+", GEC = "+str(GEC))

    #pt2 = TPaveText(0.55, 0.37, 0.78, 0.41, "BRNDC")
    pt2 = TPaveText(0.25, 0.85, 0.48, 0.89, "BRNDC")
    pt2.SetTextSize(0.05)
    pt2.SetBorderSize(1)
    pt2.SetFillColor(10)
    pt2.SetLineWidth(0)
    text2 = pt2.AddText( "LHCb simulation, PbPb")

    ## Write cut in 2D histogram
    if _var in [ "FTHitsP_5GeV" ]: 
        cut = _var.split("_")[1]
        varCut = _var.split("_")[0].split(_varTitle)[1]
        
        #cutpt = TPaveText(0.55, 0.35, 0.83, 0.4, "BRNDC")
        cutpt = TPaveText(0.15, 0.85, 0.43, 0.9, "BRNDC")
        cutpt.SetTextSize(0.04)
        cutpt.SetBorderSize(1)
        cutpt.SetFillColor(10)
        cutpt.SetLineWidth(0)
        cuttext = cutpt.AddText( varCut+">"+cut )


    topt = TPaveText(0.25, 0.75, 0.45, 0.8, "BRNDC")
    #topt = TPaveText(0.25, 0.75, 0.45, 0.8, "BRNDC")
    topt.SetTextSize(0.035)
    topt.SetBorderSize(1)
    topt.SetFillColor(10)
    topt.SetLineWidth(0)
    text = topt.AddText("Options: "+opts)

    #place_legend( canvas, x1=0.45, y1=0.22, x2=0.88, y2=0.41 )
    place_legend( canvas, x1=0.15, y1=0.65, x2=0.58, y2=0.84 )

    pt.Draw()
    pt2.Draw()
    if _var in [ "FTHitsP_5GeV" ]: cutpt.Draw()
    #topt.Draw()
    
    canvas.SetRightMargin(0.05)
    #place_legend( canvas )
    canvas.SaveAs( 'outputs/eff_plots_mymacro_'+args.outputfile+'/'+"ghostrate_vs_"+_var+"_"+tracker+'.pdf' )
    canvas.Write()


def plot_signal_and_ghosts( _var ):

    print("-- %s - %s " % (tracker, _var))

    # configure canvas
    name = "ghost rate vs. " + _var 
    title = "ghost rate vs. " + _var + ", " + tracker 
    canvas = TCanvas(name, title)

    # get histos    
    histoName = _var
    ghostName = histoName + "_Ghosts"
    ghost = repo.Get(ghostName)

    totalName = histoName + "_Total"
    total = repo.Get(totalName)

    print( ghost.GetEntries())

    try:
        assert ghost.GetEntries() > 0 and total.GetEntries() > 0
    except AttributeError:
        print("cannot find histos, continue ...")
        return

    ghost.Sumw2()
    total.Sumw2()

    # Efficnecy plot
    Signal = total.Clone( histoName + "_Signal" )
    Signal.Add( Signal, ghost, 1, -1 )

    ghost.SetTitle( "Ghost tracks" ) 
    ghost.SetName( "Ghost tracks" ) 

    Signal.SetTitle( "True tracks" )
    Signal.SetName( "True tracks" )   

    if _var in ["P"]: unit = " [MeV/c]"
    else: unit =  ""
    ghost.GetXaxis().SetTitle(_var+unit)
    ghost.GetYaxis().SetTitle("Signal and ghost yields")
    ghost.GetYaxis().SetRangeUser(1, total.GetEntries()/4 )
    ghost.Draw( "hist")
    

    gPad.SetLogy()

    #mg.SetName("Ghost rate vs. " + _var)
    #mg.SetTitle(tracker)

    rightmax = 1.05 * Signal.GetMaximum()
    scale = gPad.GetUymax() / rightmax
    Signal = Signal.Clone()
    #SetOwnership(Signal,False)
    #Signal.Scale(scale)
    #Signal.SetName("inputs")
    #Signal.SetTitle("inputs")
    Signal.SetFillStyle(3004)
    Signal.SetFillColor(kBlue)
    Signal.SetLineColor(kBlue - 10)
    Signal.SetLineWidth(2)
    Signal.Draw("hist same")
    
    #axis = TGaxis(gPad.GetUxmax(), 0, gPad.GetUxmax(), 1.05, 0,
    #              rightmax, 510, "+L")
    if _var == "P":
        axis = TGaxis(100000, 0, 100000, 1.05, 0, rightmax, 510,"+L")
        ghost.GetXaxis().SetRangeUser(0, 100000)
    if _var == "Pt":
        axis = TGaxis(5000, 0, 5000, 1.05, 0, rightmax, 510, "+L")
        ghost.GetXaxis().SetRangeUser(0, 5000)
    axis.SetTitle("Number of events")
    axis.SetTextFont(132)
    axis.SetLabelFont(132)
    axis.SetTitleFont(132)
    axis.SetLabelSize(0.06)
    axis.SetTitleSize(0.07)
    axis.SetNdivisions(505)
    gPad.SetRightMargin(0.75)
    
    # Legend
    pt = TPaveText(0.45, 0.80, 0.83, 0.95, "BRNDC")
    pt.SetTextSize(0.05)
    pt.SetBorderSize(1)
    pt.SetFillColor(10)
    pt.SetLineWidth(0)
    text = pt.AddText(tag+" tracks, GEC = "+str(GEC))

    topt = TPaveText(0.45, 0.75, 0.95, 0.8, "BRNDC")
    topt.SetTextSize(0.035)
    topt.SetBorderSize(1)
    topt.SetFillColor(10)
    topt.SetLineWidth(0)
    text = topt.AddText("Options: "+opts)

    place_legend( canvas )
    #pt.AddEntry( Signal, "True tracks", "l")
    #pt.AddEntry( ghost, "Ghost tracks", "l")

    pt.Draw()
    #topt.Draw()
    canvas.SetRightMargin(0.05)
    
    canvas.SaveAs( 'outputs/eff_plots_mymacro_'+args.file+'/'+"signalandghost_vs_"+_var+"_"+tracker+'.pdf' )
    canvas.Write()


# main macro
if __name__ == '__main__':

    parser = argument_parser()
    args = parser.parse_args()

    latex = TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.045)
    
    # get/create file
    f = TFile(args.directory + args.file+'_histo.root', 'read')
    print( args.directory + args.file+'_histo.root')
    if args.compare=="True":
        f2 = TFile(args.directory2 + args.file2+'_histo.root', 'read')

    outputfile = TFile('outputs/eff_plots_mymacro_'+args.outputfile+'.root', 'recreate')

    if not os.path.isdir( 'outputs/eff_plots_mymacro_'+args.outputfile ):
        os.system( "mkdir 'outputs/eff_plots_mymacro_"+args.outputfile+"'")
    trackers = getTrackers()
    folders  = getOriginFolders()
    tags     = getTrackTag()

    GEC = int( args.gec )
    opts = args.options
    for tracker in trackers:
        folder = folders[tracker]["folder"]
        print(folder)
        print(tracker)
        repo_name = folder + "/PrMultiplicityChecker/" +tracker
        
        outputfile.cd()
        trackerDir = outputfile.mkdir(tracker)
        trackerDir.cd()        
        #Get the dir
        try:
            repo = f.Get(repo_name)

            if not repo:
                print("not repo") # do not find the repo
                continue
        except Exception as e:
            continue
        
        if args.compare=="True":
            #Get the dir
            try:
                repo2 = f2.Get(repo_name)            
                if not repo2:
                    continue
            except Exception as e:
                continue

        # Create the efficiency histo
        for var in vars:

            g_ghost_1, deno_ghost_1 = compute_1D_ghost_rate( var, repo )
            if args.compare=="True":
                g_ghost_2, deno_ghost_2 = compute_1D_ghost_rate( var, repo2)
                plot_1D_ghost_rate( var, g_ghost_1, deno_ghost_1, g_ghost_2, deno_ghost_2 )
            else: plot_1D_ghost_rate( var, g_ghost_1, deno_ghost_1 )

            #plot_signal_and_ghosts( var )
            # Loop over the track types

            for tag in tags:
                g_eff_1, deno_1 = compute_1D_efficiency( var, repo )
                if args.compare=="True":
                    g_eff_2, deno_2 = compute_1D_efficiency( var, repo2, "ref")
                    plot_1D_efficiency( var, g_eff_1, deno_1, g_eff_2, deno_2 )

                else: plot_1D_efficiency( var, g_eff_1, deno_1 )
    f.Close()















