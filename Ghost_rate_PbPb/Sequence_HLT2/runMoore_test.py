import subprocess
import os
import argparse
import sys
from collections import OrderedDict
sys.path.insert(0, "/scratch42/ivan.cambon/Upgrade/stack/DBASE/PRConfig/python/MooreTests/") # to be adapted to path
import readTimingTable
#import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import re
from hlt2_sequence_writers import *

"""
Macro to run Moore automatically.
The goal is to have outputs for different HLT2 sequence and compare the throughputs from PbPb sample.
Exemple of commands:
    run runMoore_test.py --noplot --v --options="fast_reco:mc_monitor:no_ut"

"""

#constant
evt_max = 10 # -1 #1500 #1000 #1000 #3000 #1500 #400 #1500 #000 #500 #200 #200
gecs    = [ 30000 ]#, 60000, 50000 ] #50000 ] #, 60000 ]#40000, 80000, 60000 ]#, 70000 ] # 40000 ,80000, 100000  # GEC is UTclusters + FTclusters

fit_selections = ["(TrGHOSTPROB<0.9999)"]


#algo definition
algGroups = readTimingTable.algGroups

def process_data(algos,evt_rate,option_file):
    """
    draw the algo timing on a graph and save it
    """

    # compute couple of values
    names      = algos.keys()
    values     = algos.values()
    timing_tot = sum(values)
    try:
        fractions = [value/timing_tot*100 for value in values]
    except Exception as e:
        print("cannot compute the fraction. Returning...")
        return


    # draw the plots
    # create Pandas dataframe from two lists
    df = pd.DataFrame({"Algorithm":names, "Timing Fraction":fractions})
    df_sorted= df.sort_values('Timing Fraction')

    fig, ax = plt.subplots(figsize=(10,6))

    # make bar plot with matplotlib
    ax.barh('Algorithm', 'Timing Fraction',data=df_sorted)
    plt.xlabel("Timing Fraction (%)", size=15)
    plt.title("Throughputs HLT2", size=18)

    #add evt rate
    plt.text(7, 6, 'Evt/s = %s' % evt_rate, fontsize=15)
    fractions.sort(reverse=True)
    for i, v in enumerate(fractions):
        ax.text(v, len(fractions)-1.25 -i, "%.2f"%v)

    #add option file type
    plt.text(7, 5, '%s' % option_file.replace(".py","").replace("hlt2_option_files/hlt2_",""), fontsize=10)

    plt.savefig(option_file.replace(".py",".png").replace("hlt2_option_files","outputs"))
    plt.close()

def get_timings(name):
    # Read the filtered stdout
    # Return the algos timings

    # dict to store algos timing and name
    algos = {}
    algos_cat = algGroups["HLT2"].keys()
    for cat in algos_cat:
        algos[cat] = 0.

    evt_rate = 0.

    output_file = name.replace(".py", ".txt").replace("hlt2_option_files", "outputs")
    print(output_file)
    reader = open(output_file, 'r')
    lines = reader.readlines()
    for line in lines:
        if line.count("|") == 5:
            l = line.strip().replace(" ","").split("|")
            l.remove("")
            algo_name = l[0].replace('"','')
            for cat in algos_cat:
                for alg_name in algGroups["HLT2"][cat]:
                    if re.search(alg_name,algo_name) is not None:
                        algo_time = float(l[-2])
                        algos[cat] += algo_time
        if "Events Finished" in line:
            l = line.strip().replace(" ","").split(",")
            evt_rate = float(l[-1].split("=")[-1])
    return algos, evt_rate

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Option to tun the macro.')
    parser.add_argument("--norun",
                        help="only read output files",
                        action="store_true",
                        default=False)
    parser.add_argument("--noplot",
                        help="do not produce plots",
                        action="store_true",
                        default=False)
    parser.add_argument("--options",
                        help="option to give to hlt2_sequence_writer. keys should be seperated by :",
                        type=str,
                        default="light_reco:fast_reco")
    parser.add_argument("--v",
                        help="verbose",
                        action="store_true",
                        default=False)

    args = parser.parse_args()

    print(args.options.strip(":"))

    for gec in gecs:
        for selection in fit_selections:
            option_file = write_hlt2_sequence_option_file(gec=gec,
                                                          evt_max=evt_max,
                                                          options=args.options.split(":"),
                                                          post_fit_selection=selection)

            if args.norun is False:
                # create output files
                output_file = option_file.replace(".py",".txt").replace("hlt2_option_files", "outputs")
                if os.path.exists(output_file):
                    os.remove(output_file)

                # run the process
                print("start running Moore with %s ..." % option_file)
                process = subprocess.Popen(['/scratch42/ivan.cambon/Upgrade/stack/Moore/run', 'gaudirun.py', option_file],
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE,
                                     universal_newlines=True)

                # write the output
                with open(output_file,"w") as writer:
                    while True:
                        output = process.stdout.readline()
                        if output == '' and process.poll() is not None:
                            break
                        if output:
                            writer.write(output)
                            if args.v: print(output.strip())
                        rc = process.poll()

            if args.noplot is False:
                print("Ploting histos for %s ..." % option_file)
                algos,evt_rate = get_timings(option_file)
                process_data(algos,evt_rate,option_file)





