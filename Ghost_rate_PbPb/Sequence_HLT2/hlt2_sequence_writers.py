import subprocess
import os
import argparse
import sys
from collections import OrderedDict
sys.path.insert(0, "/scratch42/ivan.cambon/Upgrade/stack/DBASE/PRConfig/python/MooreTests/")
import readTimingTable
#import matplotlib.pyplot as plt
import Functors as F
import numpy as np
import pandas as pd
import re

"""
Sequence to write HLT2 macros to be used by runMoore_tests.py
"""

def check_options(options):
    #check the options for the writer

    keys = (
        "fast_reco", # use fast_reco
        "mc_monitor", # use mc_monitor
        "no_seeding", # skips seeding. WARNING: need to build branch in Rec: lohenry-noSeeding
        "no_seeding_mod1", # skips seeding and considers only one VELO track per long track ("DeltaQuality"=0). Need to run on Rec with the change (to be done on oboente-noSeeding)
        "no_seeding_mod2", # skips seeding and sets min PT to 200. Need to run on Rec with the change (to be done on oboente-noSeeding)
        "no_seeding_mod3", # skips seeding and sets min P to 2000. Need to run on Rec with the change (to be done on oboente-noSeeding)
        "no_seeding_mod4", # skips seeding and increases quality cut on NN response 0.14 to 0.35 (m_minQuality). Need to run on Rec with the change (to be done on oboente-noSeeding)
        "no_seeding_mod4-05", # skips seeding and increases quality cut on NN response 0.14 to 0.5 (m_minQuality). Need to run on Rec with the change (to be done on oboente-noSeeding)
        "no_ut", # use reconstruction without UT
        "mdf", # use mdf file
        "oldsim", # use simulation upgrade-202004-PbPb-EPOS-b-6_14fm # by default, use Sim10aU1_24142001
        "newsim", # use simulation upgrade-....-PbPb-EPOS-peripheral
        "d0sim", # use simulation with D0 signal Sim10aU1_22162000
        "oldtags", # use the condition and sim tags of upgrade-202004-PbPb-EPOS-b-6_14fm, but with Sim10aU1_24142001
        "nodecopt", # does not require  default_ft_decoding_version.global_bind(value=6)
        )

    for x in options:
        try:
            assert x in keys
            print(" ---> add %s option in the sequence" %x)
        except Exception as e:
            print("%s not in the possible option, will be removed." %x)
            options.remove(x)


def make_name(name, options, post_fit_selection):

    # set the name of the file
    name = name.replace(".py","_light_reco.py")
    if "mc_monitor" in options:
        name = name.replace(".py","_monitor.py")
    if "fast_reco" in options:
        name = name.replace(".py","_fast_reco.py")
    if "no_seeding" in options:
        name = name.replace(".py","_no_seeding.py")
    if "no_seeding_mod1" in options:
        name = name.replace(".py","_no_seeding_mod1.py")
    if "no_seeding_mod2" in options:
        name = name.replace(".py","_no_seeding_mod2.py")
    if "no_seeding_mod3" in options:
        name = name.replace(".py","_no_seeding_mod3.py")
    if "no_seeding_mod4" in options:
        name = name.replace(".py","_no_seeding_mod4.py")
    if "no_seeding_mod4-05" in options:
        name = name.replace(".py","_no_seeding_mod4-05.py")
    if "no_ut" in options:
        name = name.replace(".py","_no_ut.py")
    if "mdf" in options:
        name = name.replace(".py","_mdf.py")
    if "oldsim" in options:
        name = name.replace(".py","_oldsim.py")
    if "newsim" in options:
        name = name.replace(".py","_newsim.py")
    if "oldtags" in options:
        name = name.replace(".py","_oldtags.py")
    if "d0sim" in options:
        name = name.replace(".py","_d0sim.py")
    if "nodecopt" in options:
        name = name.replace(".py","_nodecopt.py")
    if post_fit_selection != None:
        name = name.replace(".py",post_fit_selection+".py").replace(" ","").replace("(","").replace(")","").replace("&","_").replace(">","greater").replace("<","lower")
    return name


def write_hlt2_sequence_option_file(gec=30000, evt_max=100, options=[], post_fit_selection=None):
    # write the hlt2 option file. Baseline is Moore/Hlt/RecoConf/options/hlt2_lead_lead_light_reco_pr_kf.py
    # configure
    check_options(options)
    name = "./hlt2_option_files/hlt2_lead_lead_evt%d_pr_kf_gec%d.py" % (evt_max, gec)
    name = make_name(name, options, post_fit_selection)

    d_post_fit_selection = {None: None, 
                            "(TrGHOSTPROB<0.9999)": 0.9999, 
                            "(TrGHOSTPROB<0.99)": 0.99 , 
                            "(TrGHOSTPROB<0.8)": 0.8,
                            "(TrGHOSTPROB<0.7)": 0.7,
                            "(TrGHOSTPROB<0.5)": 0.5,
                            "(TrGHOSTPROB<0.45)": 0.45, 
                            "(TrGHOSTPROB<0.4)": 0.4, 
                            "(TrGHOSTPROB<0.35)": 0.35, 
                            "(TrGHOSTPROB<0.3)": 0.3, 
                            "(TrGHOSTPROB<0.25)": 0.25,
                            "(TrGHOSTPROB<0.2)": 0.2,
                            "(TrGHOSTPROB<0.15)": 0.15, 
                            "(TrGHOSTPROB<0.1)": 0.1, 
                            }
    post_fit_selection = d_post_fit_selection[post_fit_selection]
    
    # remove previous file
    if os.path.exists(name):
        os.remove(name)

    #write the file
    with open(name, 'w') as writer:

        writer.write("from Moore import options, run_reconstruction\n")
        if "no_ut" in options:
            writer.write("from RecoConf.standalone import standalone_hlt2_light_reco_without_UT\n")
            writer.write("from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT\n")
        else:
            writer.write("from RecoConf.standalone import standalone_hlt2_light_reco\n")
            writer.write("from RecoConf.hlt2_tracking import make_hlt2_tracks\n")
        
        writer.write("from RecoConf.event_filters import require_gec\n")
        writer.write("from RecoConf.hlt2_tracking import make_TrackBestTrackCreator_tracks\n\n")
        writer.write("import Functors as F\n\n")

        if "mc_monitor" in options:
            writer.write("from RecoConf.mc_checking import check_track_resolution\n")

        writer.write("options.evt_max               = %d\n" % evt_max )
        writer.write("options.print_freq            = 10\n" )

        if 'mdf' in options:
            writer.write("options.use_iosvc             = True\n")
            writer.write("options.event_store           = 'EvtStoreSvc'\n")
            writer.write("options.scheduler_legacy_mode = False\n\n")
            writer.write("options.n_threads             = 8\n")
            writer.write("options.n_event_slots         = max(1.2 * options.n_threads, 1 + options.n_threads)\n")
            writer.write("options.input_files           = ['root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/upgrade-202004-PbPb-EPOS-b-6_14fm/361080073_Gausstest_MB.xdigi']\n")
            writer.write("options.conddb_tag            = 'sim-20180530-vc-md100'  \n")
            writer.write("options.dddb_tag              = 'dddb-20190223'\n")
            writer.write("options.input_type            = 'MDF'\n\n")

        else:
            # settings to read PbPb input data
            #writer.write("options.set_input_and_conds_from_testfiledb('upgrade-202004-PbPb-EPOS-b-6_14fm')\n\n")
            if 'oldsim' in options: 
                writer.write("options.set_input_and_conds_from_testfiledb('upgrade-202004-PbPb-EPOS-b-6_14fm')\n\n")            
            elif "d0sim" in options:
                writer.write("options.input_files           = ['root://x509up_u1675@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157630/0000/00157630_00000006_1.xdigi',\n")
                writer.write("                                 'root://x509up_u1675@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157630/0000/00157630_00000010_1.xdigi',\n")
                writer.write("                                 'root://x509up_u1675@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157630/0000/00157630_00000014_1.xdigi',\n")
                writer.write("                                 'root://x509up_u1675@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157630/0000/00157630_00000017_1.xdigi']\n")

                writer.write("options.conddb_tag            = 'sim-20210617-vc-mu100'  \n")
                writer.write("options.dddb_tag              = 'dddb-20210617'\n")
                writer.write("options.input_type            = 'ROOT'\n\n")
            elif "newsim" in options:
                writer.write("options.input_files           = ['root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000056_1.xdigi',\n")
                writer.write("                                 'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000162_1.xdigi',\n")
                writer.write("                                 'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000176_1.xdigi']\n")
                
                writer.write("options.conddb_tag            = 'sim-20231017-vc-md100'  \n")
                writer.write("options.dddb_tag              = 'dddb-20231017'\n")
                writer.write("options.input_type            = 'ROOT'\n\n")
            else:
                writer.write("options.input_files           = ['root://x509up_u1141@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157634/0000/00157634_00000005_1.xdigi',\n")
                writer.write("                                 'root://x509up_u1141@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157634/0000/00157634_00000007_1.xdigi',\n")
                writer.write("                                 'root://x509up_u1141@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157634/0000/00157634_00000010_1.xdigi',\n")
                writer.write("                                 'root://x509up_u1141@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00157634/0000/00157634_00000013_1.xdigi']\n")

                if "oldtags" in options:
                    writer.write("options.conddb_tag            = 'sim-20180530-vc-md100'  \n")
                    writer.write("options.dddb_tag              = 'dddb-20190223'\n")
                else:
                    writer.write("options.conddb_tag            = 'sim-20210617-vc-mu100'  \n")
                    writer.write("options.dddb_tag              = 'dddb-20210617'\n")
                writer.write("options.input_type            = 'ROOT'\n\n")

        # file of mc_monitoring
        if "mc_monitor" in options:
            writer.write("options.simulation = True\n\n")
            writer.write("options.histo_file   = '%s'\n\n" % name.replace("hlt2_option_files","monitor").replace('.py','_histo.root'))
            writer.write("options.ntuple_file   = '%s'\n\n" % name.replace("hlt2_option_files","monitor").replace('.py','.root'))
        
        writer.write("with require_gec.bind(cut=%s), \\\n" % gec)

        # mc_monitoring
        if "mc_monitor" in options:
            writer.write("\tcheck_track_resolution.bind(per_hit_resolutions=True, split_per_type=True), \\\n")
            if "no_ut" in options:
                writer.write("\tstandalone_hlt2_light_reco_without_UT.bind(do_mc_checking=True, do_data_monitoring=True), \\\n")
            else:
                writer.write("\tstandalone_hlt2_light_reco.bind(do_mc_checking=True, do_data_monitoring=True, use_pr_kf=True), \\\n")
        else:
            if "no_ut" in options:
                writer.write("\tstandalone_hlt2_light_reco_without_UT.bind(do_mc_checking=False, do_data_monitoring=True ), \\\n")
            else:
                writer.write("\tstandalone_hlt2_light_reco.bind(do_mc_checking=False, do_data_monitoring=True, use_pr_kf=True ), \\\n")


        if post_fit_selection != None:
            writer.write("\tmake_TrackBestTrackCreator_tracks.bind(max_ghost_prob={0}), \\\n".format(post_fit_selection))
            
        
        if "no_ut" in options:  
            writer.write("\tmake_hlt2_tracks_without_UT.bind(fast_reco=True, use_pr_kf=True):\\\n")  
            writer.write("\trun_reconstruction(options, standalone_hlt2_light_reco_without_UT) \n")
        else:
            writer.write("\tmake_hlt2_tracks.bind(fast_reco=True, use_pr_kf=True):\\\n")  
            writer.write("\trun_reconstruction(options, standalone_hlt2_light_reco) \n")
    return name