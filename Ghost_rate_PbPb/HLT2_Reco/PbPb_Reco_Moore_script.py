from Moore import options, run_reconstruction
from RecoConf.standalone import (standalone_hlt2_light_reco,
                                 standalone_hlt2_light_reco_without_UT)
from RecoConf.hlt2_tracking import (make_hlt2_tracks, 
                                    make_hlt2_tracks_without_UT)
from RecoConf.event_filters import require_gec
from RecoConf.hlt2_tracking import make_TrackBestTrackCreator_tracks
from RecoConf.mc_checking import check_track_resolution

# MC files and properties
conddb_tags = {"B_JpsiK"  : "sim-20231017-vc-md100",
               "Jpsi_mumu": "sim-20231017-vc-md100",
               "minbias"  : "sim-20231017-vc-md100"}

dddb_tags   = {"B_JpsiK"  : "dddb-20231017",
               "Jpsi_mumu": "dddb-20231017",
               "minbias"  : "dddb-20231017"}

input_files = {"B_JpsiK_mix": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206137/0000/00206137_00000008_1.xdigi",
                               "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207022/0000/00207022_00000082_1.xdigi",
                              ],
               
               "Jpsi_mumu_peripheral": ['root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000056_1.xdigi',
                                        'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000162_1.xdigi',
                                        'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000176_1.xdigi'
                                       ],
               
               "minbias_peripheral": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000019_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000028_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000043_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000101_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000132_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000138_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000149_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000185_1.xdigi",
                                     ]
               }

# Configurables
gec_cut      = 30000
evts         = 200
ghost_cut    = 0.1
mc_decay     = "minbias"
collision    = "peripheral"
with_UT      = True
PbPb_weights = False

options.simulation = True
options.data_type  = "Upgrade"
options.evt_max    = evts
options.print_freq = 1000

options.input_files = input_files[f"{mc_decay}_{collision}"]
options.input_type  = 'ROOT'

options.conddb_tag = conddb_tags[mc_decay]
options.dddb_tag   = dddb_tags[mc_decay]

if with_UT:
    if not PbPb_weights:
        options.histo_file  = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_sim24{mc_decay}_ghostprob{ghost_cut}_histo.root'
        options.ntuple_file = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_sim24{mc_decay}_ghostprob{ghost_cut}.root'
    else:
        options.histo_file  = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_sim24{mc_decay}_ghostprob{ghost_cut}_PbPb_weigths_histo.root'
        options.ntuple_file = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_sim24{mc_decay}_ghostprob{ghost_cut}_PbPb_weights.root'
        
else:
    if not PbPb_weights:
        options.histo_file  = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_no_ut_sim24{mc_decay}_ghostprob{ghost_cut}_histo.root'
        options.ntuple_file = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_no_ut_sim24{mc_decay}_ghostprob{ghost_cut}.root'
    else:
        options.histo_file  = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_no_ut_sim24{mc_decay}_ghostprob{ghost_cut}_PbPb_weigths_histo.root'
        options.ntuple_file = f'./monitor_files/hlt2_PbPb_evt{evts}_pr_kf_gec{gec_cut}_light_reco_monitor_fast_reco_no_ut_sim24{mc_decay}_ghostprob{ghost_cut}_PbPb_weights.root'


# Running Moore
if with_UT:
    with require_gec.bind(cut=gec_cut, skipUT=True), \
        standalone_hlt2_light_reco.bind(do_mc_checking=True, 
                                        do_data_monitoring=True, 
                                        use_pr_kf=True, 
                                        collision_type="PbPb"), \
        check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True), \
        make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=ghost_cut, PbPb=PbPb_weights):
        run_reconstruction(options, standalone_hlt2_light_reco) 
else:
    with require_gec.bind(cut=gec_cut), \
        standalone_hlt2_light_reco_without_UT.bind(do_mc_checking=True, 
                                                   do_data_monitoring=True,
                                                   collision_type="PbPb"), \
        check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True), \
        make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=ghost_cut, PbPb=PbPb_weights):
        run_reconstruction(options, standalone_hlt2_light_reco_without_UT) 
        
        