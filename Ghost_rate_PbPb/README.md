# RTA_PbPb

This project is dedicated to the RTA reconstruction work for PbPb data.
One should have a running [stack](https://gitlab.cern.ch/rmatev/lb-stack-setup) installation. Once in the `stack` repo and checkout the following branch:
```
cd Moore && git checkout RTA_audurier && cd ..
```

The above will create a new local branch named `RTA_audurier` linked to the remote branch `origin/RTA_audurier`.
Note that the code might not be up-to-date w.r.t `origin/master`. In that case, the following should be done:

1. Checkout to local `master` branch in all projects.
2. Go to the `stack` repo and do `make update`.
3. Go back to the dev repo (ex: `cd Moore`), rebase the dev branch to `master` (ex: `git checkout RTA_audurier && git rebase master`).

Once this is done for all repositories developed, go pack to `stack`, purge the project (`make purge`) and recompile. 

Codes in `Sequence_HLT2` is used to run the HLT2 sequences. See the code for details, and probably change some hard-coded paths.
If everything is installed, the following command should work from `Sequence_HLT2` within a `ipython` session:

```
./path/to/stack/Moore/run runMoore_test.py --noplot --v --options="fast_reco:selmatch:mc_monitor:no_ut"
```

NOTE: it might be advisable to enter the ipython terminal from the ./run of Moore:
```
./path/to/stack/Moore/run ipython
```
in this way, problems with the built-in python installation of lxplus are avoided. Then, within the ipython terminal:
```
run runMoore_test.py --noplot --v --options="fast_reco:selmatch:mc_monitor:no_ut"
```

NOTE: The actual version of this branch is thought to run the `option_file_v1.py` script through the following command

```
./path/to/stack/Moore/run gaudirun.py option_file_v1.py
```

The `Sequence_HLT2` scripts will be modified to include the changes made in the `option_file_v1.py`