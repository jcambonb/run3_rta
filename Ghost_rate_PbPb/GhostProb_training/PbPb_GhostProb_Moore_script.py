from PyConf import configurable
from functools import partial

from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from PyConf.Algorithms import PrHybridSeeding
from RecoConf.legacy_rec_hlt1_tracking import (make_reco_pvs, make_PatPV3DFuture_pvs)

from RecoConf.hlt2_global_reco import reconstruction, make_fastest_reconstruction, make_light_reco_pr_kf_without_UT, make_light_reco_pr_kf
from RecoConf.data_from_file import mc_unpackers
from RecoConf.mc_checking import make_links_lhcbids_mcparticles_tracking_system, make_links_tracks_mcparticles
from RecoConf.event_filters import require_gec

from RecoConf.hlt2_tracking import  (
    make_PrKalmanFilter_Seed_tracks, 
    make_PrKalmanFilter_Velo_tracks,
    make_TrackBestTrackCreator_tracks)

from PyConf.Algorithms import (
    GhostProb_Long_noUT_TrainingTupleAlg, GhostProb_Long_TrainingTupleAlg,
    GhostProb_Ttrack_TrainingTupleAlg, GhostProb_Downstream_TrainingTupleAlg,
    GhostProb_Upstream_TrainingTupleAlg, GhostProb_Velo_TrainingTupleAlg)


# Moore Algorithms
def get_tuple_alg(track_type, with_UT=False):
    if track_type == 'Long':
        return GhostProb_Long_TrainingTupleAlg if with_UT else GhostProb_Long_noUT_TrainingTupleAlg
    elif track_type == 'Downstream':
        return GhostProb_Downstream_TrainingTupleAlg
    elif track_type == 'Upstream':
        return GhostProb_Upstream_TrainingTupleAlg
    elif track_type == 'Ttrack':
        return GhostProb_Ttrack_TrainingTupleAlg
    elif track_type == 'Velo':
        return GhostProb_Velo_TrainingTupleAlg
    return None

@configurable
def standalone_hlt2_tracks(
    models={'Long_noUT': {'type': 'Long',
                          'weights': '',
                          }
            },
    make_reconstruction=make_fastest_reconstruction):
    
    reco = reconstruction(make_reconstruction=make_reconstruction)
    mc_parts = mc_unpackers()['MCParticles']
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    
    data = []
    
    for key, value in models.items():
        reco_name = value['reco_obj']
        tracks = reco.get(reco_name, None)
        if tracks is None: tracks = reco['AllTrackHandles'][reco_name]['v1']
        links_to_tracks = make_links_tracks_mcparticles(tracks,
                                                        LinksToLHCbIDs=links_to_lhcbids)
        
        tuple_alg = get_tuple_alg(track_type=value['type'], with_UT='noUT' not in key)
        
        training_tuple = tuple_alg(name=f"TrainingTupleAlg_GhostProb_{key}",
                                   InputObjects=tracks,
                                   MCParticles=mc_parts,
                                   LinksToMC=links_to_tracks,
                                   WeightsFileName=value['weights'])
        
        data.append(training_tuple)
        
    prefilters = [require_gec()]
        
    return Reconstruction('hlt2_tracks', data, prefilters)

# MC files and properties
conddb_tags = {"B_JpsiK"  : "sim-20231017-vc-md100",
               "Jpsi_mumu": "sim-20231017-vc-md100",
               "minbias"  : "sim-20231017-vc-md100"}

dddb_tags   = {"B_JpsiK"  : "dddb-20231017",
               "Jpsi_mumu": "dddb-20231017",
               "minbias"  : "dddb-20231017"}

input_files = {"B_JpsiK_mix": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00206137/0000/00206137_00000008_1.xdigi",
                               "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207022/0000/00207022_00000082_1.xdigi",
                              ],
               
               "Jpsi_mumu_peripheral": ['root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000056_1.xdigi',
                                        'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000162_1.xdigi',
                                        'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00216522/0000/00216522_00000176_1.xdigi'
                                        ],
               
               "minbias_peripheral": ["root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000019_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000028_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000043_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000101_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000132_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000138_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000149_1.xdigi",
                                      "root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00207020/0000/00207020_00000185_1.xdigi",
                                     ]
               }

# Configurables
gec_cut      = 30000
evts         = 20
mc_decay     = "minbias"
collision    = "peripheral"
with_UT      = True
PbPb_weights = True  

if evts != -1:
    if PbPb_weights:
        options.ntuple_file = f'./root_files/PbPb_{collision}_ghostprob_training_tuple_gec_{gec_cut}_evts_{evts}_{mc_decay}_PbPb_weights.root'
    else:
        options.ntuple_file = f'./root_files/PbPb_{collision}_ghostprob_training_tuple_gec_{gec_cut}_evts_{evts}_{mc_decay}.root'
else:
    if PbPb_weights:
        options.ntuple_file = f'./root_files/PbPb_{collision}_ghostprob_training_tuple_gec_{gec_cut}_{mc_decay}_PbPb_weigths.root'
    else:
        options.ntuple_file = f'./root_files/PbPb_{collision}_ghostprob_training_tuple_gec_{gec_cut}_{mc_decay}.root'

options.simulation = True
options.data_type  = "Upgrade"
options.evt_max    = evts
options.print_freq = 1000

options.input_files = input_files[f"{mc_decay}_{collision}"]
options.input_type  = 'ROOT'

options.conddb_tag = conddb_tags[mc_decay]
options.dddb_tag   = dddb_tags[mc_decay]

if not PbPb_weights:
    models = {'Long_noUT': {'type': 'Long',
                            'reco_obj': 'LongTracks',
                            'weights': 'paramfile:///data/GhostProbability/hlt2_ghostprob_long_noUT.json',
                            },


              'Ttrack_noUT': {'type': 'Ttrack',
                              'reco_obj': 'BestSeed',
                              'weights': 'paramfile:///data/GhostProbability/hlt2_ghostprob_ttrack_noUT.json',
                             },

              'Long':      {'type': 'Long',
                            'reco_obj': 'LongTracks',
                            'weights': 'paramfile:///data/GhostProbability/hlt2_ghostprob_long.json',
                            },

             "Upstream": {'type': 'Upstream',
                           'reco_obj': 'UpstreamTracks',
                           'weights': 'paramfile:///data/GhostProbability/hlt2_ghostprob_upstream.json',
                          },

             "Downstream": {'type': 'Downstream',
                           'reco_obj': 'DownstreamTracks',
                           'weights': 'paramfile:///data/GhostProbability/hlt2_ghostprob_downstream.json',
                          },

             'Velo': {'type': 'Velo', 
                     'reco_obj': 'BestVelo',
                     'weights': 'paramfile:///data/GhostProbability/hlt2_ghostprob_velo_noUT.json',
                    },

            }

else:
    models = {'Long_noUT': {'type': 'Long',
                            'reco_obj': 'LongTracks',
                            'weights': '',
                            },
              
              'Ttrack_noUT': {'type': 'Ttrack',
                              'reco_obj': 'BestSeed',
                              'weights': '',
                             },
              
              'Long':      {'type': 'Long',
                            'reco_obj': 'LongTracks',
                            'weights': '',
                            },
              
              "Upstream": {'type': 'Upstream',
                           'reco_obj': 'UpstreamTracks',
                           'weights': '',
                          },
              
             "Downstream": {'type': 'Downstream',
                           'reco_obj': 'DownstreamTracks',
                           'weights': '',
                          },
              
             'Velo': {'type': 'Velo', 
                     'reco_obj': 'BestVelo',
                     'weights': '',
                    },
              
              }

if with_UT:
    with require_gec.bind(cut=gec_cut), \
        PrHybridSeeding.bind(RemoveBeamHole=True, RemoveClones_forLead=True),\
        make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
        make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
        standalone_hlt2_tracks.bind(make_reconstruction=make_light_reco_pr_kf):
        run_reconstruction(options, partial(standalone_hlt2_tracks, models=models))
else:
    with require_gec.bind(cut=gec_cut), \
        standalone_hlt2_tracks.bind(make_reconstruction=make_light_reco_pr_kf_without_UT):
        run_reconstruction(options, partial(standalone_hlt2_tracks, models=models))