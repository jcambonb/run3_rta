import numpy as np 
import torch, random
import matplotlib.pyplot as plt
import ROOT as R
import pandas as pd
from datetime import datetime

#import mplhep
#import sys 
#import os
#
#mplhep.styles.use(mplhep.styles.LHCb2)


# deterministic behaviour
det = True
seed = 0
generator = torch.Generator()
epochs = 100
verbose = True
save = True

if det:
    torch.use_deterministic_algorithms(True)
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    generator.manual_seed(seed)

def seed_worker(worker_id):
    worker_seed = torch.initial_seed( ) % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)
    
# import data

path = "./root_files"
file = "PbPb_peripheral_ghostprob_training_tuple_gec_30000_minbias.root"
model_names = ["Long", "Upstream", "Downstream", "Velo"]

rdfs = [R.RDataFrame(f'TrainingTupleAlg_GhostProb_{name}/TrainingTuple',
                     f"{path}/{file}") for name in model_names]


# import model
def get_model(name):
    if name == 'Long_noUT':
        from TrackTools.ghostprob_models import GhostProb_Long_noUT as model
        return model
    elif name == 'Long':
        from TrackTools.ghostprob_models import GhostProb_Long as model
        return model
    elif name == 'Downstream':
        from TrackTools.ghostprob_models import GhostProb_Downstream as model
        return model
    elif name == 'Upstream':
        from TrackTools.ghostprob_models import GhostProb_Upstream as model
        return model
    elif name == 'Ttrack':
        from TrackTools.ghostprob_models import GhostProb_Ttrack as model
        return model
    elif name == 'Velo':
        from TrackTools.ghostprob_models import GhostProb_Velo as model
        return model
    return None

models = [get_model(name) for name in model_names]
feats_per_model = [model.features() for model in models]


for name, model, feats in zip (model_names, models, feats_per_model):
    print("===========================================================")
    print(f"Defined model '{name}' with features:", feats)
    print("model converted to PyTorch:", model)


others = ['MC_TRUEID', 'MC_MOTHER_ID', 'MC_GMOTHER_ID', 'MC_TRUE_P']

obs_per_model = [feats + others for feats in feats_per_model]

dfs = [pd.DataFrame(rdf.AsNumpy(obs)) for rdf, obs in zip(rdfs, obs_per_model)]

labels = ["sig", "bkg"]

selected_dfs = [ ]

for (df, feats, name) in zip(dfs, feats_per_model, model_names): 
    base_sel = (df["MC_TRUE_P"] > -1)
    df['sig']  = np.where((np.abs(df["MC_TRUEID"]) != 0) & (np.abs(df['MC_TRUEID']) != 11), 
                           True, 
                           False)
    df['bkg']  = np.where((np.abs(df['MC_TRUEID']) == 0), True, False)
    df['flag'] = np.where(df['sig'], True, False)
    
    selected_df = df[(df['sig'] | df['bkg']) & base_sel]
    
    selected_dfs.append(selected_df)

    for feat in feats:
        plt.figure(1)
        plt.hist((df.query("sig == True"))[feat], density=True, bins=100, 
                 color="black", histtype="step", label="Track")
        plt.hist((df.query("bkg == True"))[feat], density=True, bins=100, 
                 color="red", histtype="step", label="Ghost Track")
        plt.xlabel(feat)
        plt.ylabel("A.U")
        plt.legend()
        plt.savefig(f"./plots/Sig_bkg_comp_{feat}_{name}.pdf", dpi=300, bbox_inches='tight')
        plt.clf()


datafeats = [df[feats] for df, feats in zip(selected_dfs, feats_per_model)]
dataflags = [df[['flag']] for df in selected_dfs] 

nsigs = [df['sig'].sum() for df in selected_dfs]
nbkgs = [df['bkg'].sum() for df in selected_dfs]

for name, nsig, nbkg in zip(model_names, nsigs, nbkgs):
    print("===========================================================")
    print(f"Defined model '{name}'")
    print(f"Number of signal is {nsig}; number of background: {nbkg}") 
    
    
import copy
from torch import nn
from torch.utils.data import DataLoader, TensorDataset


X = [torch.tensor(datafeat.values, dtype=torch.float32) for datafeat in datafeats]
y = [torch.tensor(dataflag.values, dtype=torch.float32) for dataflag in dataflags]


from sklearn.model_selection import train_test_split

X_trains = [ ]; X_tests = [ ]
y_trains = [ ]; y_tests = [ ] 

for x, y in zip(X, y):
    X_train, X_test, y_train, y_test = train_test_split(x, y,
                                                        train_size=0.7,
                                                        shuffle=True,
                                                        random_state=seed if det else None)
    
    X_trains.append(X_train); X_tests.append(X_test)
    y_trains.append(y_train); y_tests.append(y_test)
    

def train(dataloader, model, loss_func, 
          epochs=10, learning_rate=1e-3, X_val=None, y_val=None, verbose=True):
    
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    best_acc = -np.inf
    best_weights = None

    print(f"Starting training (with {epochs} epochs)")
    for epoch in range(epochs):
        model.train()
        for inputs, targets in dataloader:
            # forward pass
            prediction = model(inputs)
            loss = loss_func(prediction, targets)
            # backward pass
            optimizer.zero_grad()
            loss.backward()
            # update weights
            optimizer.step()
        # accuracy evaluation at each epoch
        model.eval()
        prediction = model(X_val)
        acc = (prediction.round() == y_val).float().mean()
        acc = float(acc)
        if verbose and (epoch < 10 or epoch % 10 == 0):
            print(f"epoch #{epoch:3}: accuracy is {acc:.5f}")
        if acc > best_acc:
            best_acc = acc
            best_weights = copy.deepcopy(model.state_dict())

    # restore model and return best accuracy
    model.load_state_dict(best_weights)
    
batch_size = 64
train_datasets = [TensorDataset(X_train, y_train) for X_train, y_train in zip(X_trains, y_trains)]

train_dataloaders = [DataLoader(train_dataset, 
                                batch_size=batch_size, 
                                worker_init_fn=seed_worker,
                                generator=generator) for train_dataset in train_datasets]


from sklearn.metrics import roc_curve, roc_auc_score

files_names = {"Long"      : "long",
               "Upstream"  : "upstream",
               "Downstream": "downstream", 
               "Velo"      : "velo"}

for train_dataloader, X_test, y_test, model, name in zip(train_dataloaders, 
                                                                X_tests, 
                                                                y_tests,
                                                                models, 
                                                                model_names):
    
    ti = datetime.now()
    
    print("===========================================================")
    print(f"Training model '{name}'")
    
    train(train_dataloader, 
          model,
          loss_func=nn.BCELoss(),
          epochs=epochs,
          X_val=X_test,
          y_val=y_test,
          verbose=epochs)

    pred_test = model(X_test)
    output = pred_test.cpu().detach().numpy().transpose()[0]
    flags = y_test.numpy().transpose()[0]
    pred_dataset = pd.DataFrame(data={"output": output, 'flag': flags})

    fpr, tpr, thresholds = roc_curve(pred_dataset['flag'], pred_dataset['output'])
    auc = roc_auc_score(pred_dataset['flag'], pred_dataset['output'])

    print(f"Result of training: AUC of ROC = {auc:.3f}")

    plt.figure(2)
    plt.plot(fpr, tpr, label=f'(AUC = {auc:.2f})', color="red")
    plt.plot([0, 1], [0, 1], color='black', linestyle='--')
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc='lower right')
    plt.gca().set_aspect('equal', adjustable='box')
    plt.savefig(f"./plots/GhostProb_roc_curve_PbPb_{name}.pdf", dpi=300, bbox_inches='tight')
    plt.clf()

    # add flattening layer
    flatten_data = pred_dataset[pred_dataset['flag'] == 0]
    model.add_flatten_layer(flatten_data['output'], [0, 1], smoothen=True)

    # save weights
    
    if save: 
        json_file = f"hlt2_ghostprob_PbPb_{files_names[name]}.json"
        model.write_to_json(f"./json_files/{json_file}")
    
    tf = datetime.now()

    print(f"Execution time = {tf-ti}")
    print("===========================================================")