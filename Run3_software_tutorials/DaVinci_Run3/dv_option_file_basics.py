from PyConf.reading import get_particles, get_pvs

from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
import Functors as F

def main(options: Options):
    line = "Hlt2_JpsiToMuMu"
    data = get_particles(f"/Event/HLT2/{line}/Particles")
    line_prefilter = create_lines_filter(name=f"PreFilter_{line}", lines=[line])
    
    pvs = get_pvs()
    
    fields = {"Jpsi": "J/psi(1S) -> mu+ mu-",
              "mup" : "J/psi(1S) -> ^mu+ mu-",
              "mum" : "J/psi(1S) -> mu+ ^mu-"}
    
    global_features = FunctorCollection({"M": F.MASS,
                                         "P": F.P,
                                         "PT": F.PT})
    
    features = {"ALL": global_features}
    
    dtt = Funtuple(
        name="Jpsimumu",
        tuple_name="DecayTree",
        fields=fields,
        variables=features,
        inputs=data,
    )
    
    algs = {line: [line_prefilter, dtt]}
    
    return make_config(options, algs)