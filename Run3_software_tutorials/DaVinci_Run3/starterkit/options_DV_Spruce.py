###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.reading import get_particles, get_pvs

from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
import Functors as F


def main(options: Options):
    line = "SpruceRD_BuToKpEE"
    data = get_particles(f"/Event/Spruce/{line}/Particles")
    line_prefilter = create_lines_filter(name=f"PreFilter_{line}", lines=[line])

    pvs = get_pvs()

    fields = {
        'Bu'  : "[B+ -> (J/psi(1S) -> e+ e-) K+]CC",
        'Jpsi': "[B+ -> ^(J/psi(1S) -> e+ e-) K+]CC",
        'ep'  : "[B+ -> (J/psi(1S) -> ^e+ e-) K+]CC",
        'em'  : "[B+ -> (J/psi(1S) -> e+ ^e-) K+]CC",
        'Kp'  : "[B+ -> (J/psi(1S) -> e+ e-) ^K+]CC"
    }

    variables = FunctorCollection(
        {
            "M": F.MASS,
            "ID": F.PARTICLE_ID,
            "PX": F.PX,
            "PY": F.PY,
            "PZ": F.PZ,
            "ENERGY": F.ENERGY,
            "Q": F.CHARGE,
        }
    )
    muon_variables = FunctorCollection(
        {
            "ISMUON": F.ISMUON,
            "BPVIP": F.BPVIP(pvs),
            "BPVIPCHI2": F.BPVIPCHI2(pvs),
            "TRCHI2": F.CHI2,
        }
    )

    Hlt2_decisions = ["Hlt2Topo2Body", "Hlt2Topo3Body"]
    variables+=FC.HltTisTos(
        selection_type="Hlt2",
        trigger_lines=[f"{x}Decision" for x in Hlt2_decisions],
        data=data
        )

    branches = {"ALL": variables}#, "mup": muon_variables, "mum": muon_variables}

    event_info = FC.EventInfo()

    funtuple = Funtuple(
        name="myTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=branches,
        inputs=data,
        event_variables=event_info,
    )

    algs = {line: [line_prefilter, funtuple]}

    return make_config(options, algs)
