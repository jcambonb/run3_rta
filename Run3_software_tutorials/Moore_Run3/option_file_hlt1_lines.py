from Moore.config import run_allen
from Moore.options import options
from RecoConf.hlt1_allen import allen_gaudi_config as allen_sequence
from RecoConf.decoders import default_ft_decoding_version

default_ft_decoding_version.global_bind(value=4)
options.set_input_and_conds_from_testfiledb("exp_24_minbias_Sim10c_magdown")
options.evt_max = 100
options.output_file = "HLT1.dst"
options.output_type = "ROOT"

with allen_sequence.bind(sequence="hlt1_pp_forward_then_matching_no_ut"):
    run_allen(options)