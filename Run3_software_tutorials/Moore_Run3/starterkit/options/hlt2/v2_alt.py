from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.qee.dimuon_no_ip import turbo_lines, full_lines

options.set_input_and_conds_from_testfiledb("exp_24_minbias_Sim10c_magdown")
options.evt_max = 100
options.output_type = 'MDF'
options.output_file = "hlt2_v2_alt_output.mdf"
options.output_manifest_file = "hlt2_v2_alt_output.tck.json"


def make_lines():
    return [
        builder() for line_dict in [turbo_lines, full_lines]
        for builder in line_dict.values()
    ]


with reconstruction.bind(from_file=False):
    run_moore(options, make_lines)
