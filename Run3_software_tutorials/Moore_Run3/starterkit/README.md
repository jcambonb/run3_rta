# LHCbStarterKit Feb24
## Moore line development - Run3

## Example
Intended to be ran on Moore v55r0 like so. (Requires an lhcb environment)

```
lb-run -c x86_64_v3-el9-gcc13+detdesc-opt+g Moore/v55r0 gaudirun.py options/hlt2/v0.py 2>&1 | tee hlt2_v0.log
```
### LHCb environment initialisiation
```
source /cvmfs/lhcb.cern.ch/lib/LbEnv
lhcb-proxy-init
kinit <username>@CERN.CH
```