from Moore import options, run_moore
from RecoConf.decoders import default_ft_decoding_version
from Hlt2Conf.lines.test.hlt2_test import dzero2kpkm_line, dstarp2dzeropip_dzero2kpkm_line, b2d0mu_dzero2kpkm_line
from Hlt2Conf.lines.charmonium_to_dimuon import JpsiToMuMu_line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_fastest_reconstruction

input_files = ['root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000003_1.digi',
               'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000040_1.digi',
               'root://x509up_u147315@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00217991/0000/00217991_00000042_1.digi']

# Jpsi to mumu proton-proton files
options.input_files = input_files
options.input_type = 'ROOT'

options.evt_max = 100
options.print_freq = 360

options.simulation = True
options.data_type = "Upgrade"
options.conddb_tag = 'sim-20231017-vc-md100'  
options.root_ioalg_name = 'RootIOAlgExt'
options.root_ioalg_opts = {'IgnorePaths': ['/Event/Rec/Summary']}
options.dddb_tag   = 'dddb-20231017'
options.geometry_version = 'run3/trunk'
options.conditions_version = 'master'
#options.msg_svc_format = "% F%56W%S%7W%R%T %0W%M"

options.output_type = 'ROOT'
options.output_file = "./dst_files/hlt2_try_output.dst"
options.output_manifest_file = "./manifest_files/hlt2_try_output.tck.json"


#from RecoConf.muonid import make_muon_hits
#make_muon_hits.global_bind(geometry_version=3)

def make_lines():
    return [JpsiToMuMu_line()]

# bind allows you to perform by yourself the reconstruction with Moore

# from_file = True is for Sprucing
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines)

# how to get the rate from the log HL1 approx 1 MHz 
# Rate = input_rate (1.14 MHz) * retention
# BW = Rate * [#Event Size (B) / # ran over]
# BW = input_rate * Total Size (B) / #ran over
# Test in min bias MC. With ls -lh <output.mdf>